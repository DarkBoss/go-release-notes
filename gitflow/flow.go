package gitflow

import (
	"fmt"
	"regexp"
	"strings"
)

const (
	DefaultCharset = `0-9\w\-_\.`
	DefaultBranchNamingFormat = `(?m)(([0-9\w\-_\.\:]+)/([` + DefaultCharset + `]+))([^$]+|)`
	MessageTrimFormat = `^[\s\-_|:]+`
	ExtractLinkedEntityIdFormat = `^([0-9\.]+|[\w\-]+)`
)

var ExtractLinkedEntityId = regexp.MustCompile(ExtractLinkedEntityIdFormat)
var MessageTrimRule = regexp.MustCompile(MessageTrimFormat)

type BranchMatchResult struct {
	FlowName       string
	BranchName     string
	LinkedEntity   string
	LinkedEntityId string
	Message        string
	Errors         []error
}

func NewEmptyBranchMatchResult() *BranchMatchResult {
	return NewBranchMatchResult("", "", "", "", "")
}

func NewBranchMatchResult(flow, branch, linkedEntity, linkedEntityId, message string, errors ...error) *BranchMatchResult {
	return &BranchMatchResult{
		flow,
		branch,
		linkedEntity,
		linkedEntityId,
		TrimMessage(message),
		errors,
	}
}

func CloneBranch(b *Branch) *Branch {
	dup := &Branch{}
	*dup = *b
	return dup
}

func GetDefaultBranch(t Type) *Branch {
	bs := GetDefaultBranches()
	return CloneBranch(bs[uint8(t)])
}

func GetDefaultBranches() BranchMap {
	dup := func(v *Branch) *Branch {
		b := &Branch{}
		*b = *v
		return b
	}
	return BranchMap{
		uint8(NoType):      dup(NoBranch),
		uint8(TypeBugfix):  dup(BranchBugfix),
		uint8(TypeHotfix):  dup(BranchHotfix),
		uint8(TypeFeature): dup(BranchFeature),
		uint8(TypeSupport): dup(BranchSupport),
		uint8(TypeRelease): dup(BranchRelease),
	}
}

type Flow struct {
	BranchNamingFormat string
	BranchNamingRule   *regexp.Regexp
	Branches           map[uint8]*Branch
}

func NewFlowWithDefaults() *Flow {
	return NewFlow(GetDefaultBranches())
}

func NewFlow(branches map[uint8]*Branch) *Flow {
	return &Flow{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), branches}
}

func (this *Flow) GetBranchByName(name string) *Branch {
	lname := strings.ToLower(name)
	found := this.FilterBranches(func(branch *Branch) bool {
		return strings.ToLower(branch.Name()) == lname
	})
	if len(found) == 0 {
		return nil
	}
	return found[0]
}

func (this *Flow) GetBranch(t Type) *Branch {
	found := this.FilterBranches(func(b *Branch) bool { return b.Type() == t })
	if len(found) == 0 {
		return nil
	}
	return found[0]
}

func (this *Flow) FilterBranches(predicate func(*Branch) bool) []*Branch {
	ret := []*Branch{}
	for _, b := range this.Branches {
		if predicate(b) {
			ret = append(ret, b)
		}
	}
	return ret
}

func (this *Flow) SupportedBranchNames() []string {
	ret := []string{}
	for _, n := range this.Branches {
		ret = append(ret, n.name)
	}
	return ret
}

func (this *Flow) Match(messageOrBranchName string, res *BranchMatchResult) bool {
	var matches [][]string
	var errs = []error{}
	var ret = false
	var flowName, branchName, linkedEntity, linkedEntityId, message string
	if len(messageOrBranchName) == 0 {
		return ret
	}
	matches = this.BranchNamingRule.FindAllStringSubmatch(messageOrBranchName, -1)
	//fmt.Printf("'%v' matches '%v' -> %v\n", messageOrBranchName, this.BranchNamingRule.String(), matches)
	if len(matches) > 0 && len(matches[0]) == 5 {
		match := matches[0]
		flowName = match[2]
		flow := this.GetBranchByName(flowName)
		if flow == nil {
			errs = append(errs, fmt.Errorf("%s: unknown flow type, available: %v", flowName, this.SupportedBranchNames()))
		}
		branchName = match[1]
		linkedEntity = match[3]
		message = match[4]
		// name of the gitflow branch, is flow name part of full branch name
		if strings.TrimSpace(linkedEntity) == "" {
			errs = append(errs, fmt.Errorf("missing linked entity identifier"))
		}
		branch := this.GetBranchByName(flowName)
		if branch == nil {
			errs = append(errs, fmt.Errorf("sucessful extraction but unknown gitflow type '%s'", flowName))
		}
		linkedEntityIdParts := ExtractLinkedEntityId.FindAllStringSubmatch(linkedEntity, -1)
		if len(linkedEntityIdParts) > 0 {
			linkedEntityId = linkedEntityIdParts[0][1]
		}
		message = TrimMessage(message)
		ret = true
	}
	if res != nil {
		*res = *NewBranchMatchResult(flowName, branchName, linkedEntity, linkedEntityId, message, errs...)
	}
	return ret
}
func TrimMessage(s string) string {
	return MessageTrimRule.ReplaceAllString(s, "")
}

func (this *Flow) Extract(messageOrBranchName string) (ok bool, flow *Branch, branchName, linkedEntity, linkedEntityId, message string, err error) {
	errs := []string{}
	matchRes := BranchMatchResult{}
	ret := false
	flow = this.GetBranch(NoType)
	if this.Match(messageOrBranchName, &matchRes) {
		flow = this.GetBranchByName(matchRes.FlowName)
		if flow == nil {
			errs = append(errs, fmt.Sprintf("%s: unknown flow type, available: %v", matchRes.FlowName, this.SupportedBranchNames()))
		}
		branchName = matchRes.BranchName
		branchNameMessageTrimRule := regexp.MustCompile(`^` + branchName + `:[\s]*`)
		message = branchNameMessageTrimRule.ReplaceAllString(messageOrBranchName, "")
		linkedEntity = matchRes.LinkedEntity
		linkedEntityId = matchRes.LinkedEntityId
		ret = true
	}
	if len(errs) > 0 {
		if len(errs) == 1 {
			err = fmt.Errorf("failed to extract gitflow informations: %s", errs[0])
		} else {
			err = fmt.Errorf("failed to extract gitflow informations:\n\t- %s", strings.Join(errs, "\n\t- "))
		}
	}
	if len(strings.TrimSpace(message)) == 0 {
		message = messageOrBranchName
	}
	return ret, flow, branchName, linkedEntity, linkedEntityId, message, err
}
