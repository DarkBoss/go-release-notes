package gitflow

import (
	"bitbucket.org/DarkBoss/go-release-notes/git"
)

// BranchMap maps FlowTypes to their Branch counterparts
type BranchMap = map[uint8]*Branch

var (
	NoBranch      = &Branch{NoType, "none"}
	BranchBugfix  = &Branch{TypeBugfix, "bugfix"}
	BranchHotfix  = &Branch{TypeHotfix, "hotfix"}
	BranchFeature = &Branch{TypeFeature, "feature"}
	BranchSupport = &Branch{TypeSupport, "support"}
	BranchRelease = &Branch{TypeRelease, "release"}
)

// The TypeDetector handler detects the type of a specific commit.
// It tries to match the commit's message with a custom regexp.
type BranchDetector func(commit *git.Commit) (bool, interface{})

// Represent a git log entry Branch.
// It allows categorization of commits.
type Branch struct {
	_type Type
	name  string
}

// Create a new Branch with ids
func NewBranch(t Type, name string) *Branch {
	return &Branch{t, name}
}

// Return the Branch id
func (this Branch) Type() Type {
	return this._type
}

// Return the Branch human name
func (this Branch) Name() string {
	return this.name
}

// Stringify the Branch
func (this Branch) String() string {
	return this.Type().String()
}
