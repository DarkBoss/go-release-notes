package gitflow

import (
	"reflect"
	"regexp"
	"testing"
)

func TestFlow_Match(t *testing.T) {
	type fields struct {
		BranchNamingFormat string
		BranchNamingRule   *regexp.Regexp
		Branches           map[uint8]*Branch
	}
	type args struct {
		messageOrBranchName string
		res                 *BranchMatchResult
	}
	type test struct {
		name     string
		fields   fields
		args     args
		want     bool
		expected *BranchMatchResult
	}
	createDefaultFields := func() fields {
		return fields{
			DefaultBranchNamingFormat,
			regexp.MustCompile(DefaultBranchNamingFormat),
			GetDefaultBranches(),
		}
	}
	createResult := func(flow, linked, linkedId, message string) *BranchMatchResult {
		return &BranchMatchResult{
			flow,
			flow + "/" + linked,
			linked,
			linkedId,
			message,
			[]error{},
		}
	}
	tests := []test{
		{
			"simple commit message shouldn't match",
			createDefaultFields(),
			args{
				"im a simple commit message",
				&BranchMatchResult{},
			},
			false,
			&BranchMatchResult{},
		},
		{
			"bugfix commit message should match",
			createDefaultFields(),
			args{
				"bugfix/444556: fix stupid shit",
				&BranchMatchResult{},
			},
			true,
			createResult("bugfix", "444556", "444556", "fix stupid shit"),
		},
		{
			"hotfix commit message should match",
			createDefaultFields(),
			args{
				"hotfix/123334: defect blocking prod",
				&BranchMatchResult{},
			},
			true,
			createResult("hotfix", "123334", "123334", "defect blocking prod"),
		},
		{
			"feature commit message should match",
			createDefaultFields(),
			args{
				"feature/feature_a: its a me, mario",
				&BranchMatchResult{},
			},
			true,
			createResult("feature", "feature_a", "feature_a", "its a me, mario"),
		},
		{
			"support commit message should match",
			createDefaultFields(),
			args{
				"support/my-implementation: super impl solving issues",
				&BranchMatchResult{},
			},
			true,
			createResult("support", "my-implementation", "my-implementation", "super impl solving issues"),
		},
		{
			"release commit message should match",
			createDefaultFields(),
			args{
				"release/1.0.0.0: Super duper update",
				&BranchMatchResult{},
			},
			true,
			createResult("release", "1.0.0.0", "1.0.0.0", "Super duper update"),
		},
	}
	checkResult := func(got bool, tst *test) bool {
		actual := tst.args.res
		expect := tst.expected
		actual.Errors = []error{}
		expect.Errors = []error{}
		return got == tst.want && reflect.DeepEqual(actual, expect)
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Flow{
				BranchNamingFormat: tt.fields.BranchNamingFormat,
				BranchNamingRule:   tt.fields.BranchNamingRule,
				Branches:           tt.fields.Branches,
			}
			if got := this.Match(tt.args.messageOrBranchName, tt.args.res); !checkResult(got, &tt) {
				t.Errorf("Flow.Match() = %v, want %v\n\t\tactual: %v\n\t\texpected: %v", got, tt.want, tt.args.res, tt.expected)
			} else {
				t.Logf("Flow.Match('%s') = %v", tt.args.messageOrBranchName, tt.args.res)
			}
		})
	}
}

func TestFlow_GetBranchByName(t *testing.T) {
	type fields struct {
		BranchNamingFormat string
		BranchNamingRule   *regexp.Regexp
		Branches           map[uint8]*Branch
	}
	type args struct {
		name string
	}
	branches := GetDefaultBranches()
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Branch
	}{
		{"GetBranchByName() should return bugfix flow", fields{"", nil, branches}, args{branches[uint8(TypeBugfix)].Name()}, &Branch{TypeBugfix, branches[uint8(TypeBugfix)].Name()}},
		{"GetBranchByName() should return hotfix flow", fields{"", nil, branches}, args{branches[uint8(TypeHotfix)].Name()}, &Branch{TypeHotfix, branches[uint8(TypeHotfix)].Name()}},
		{"GetBranchByName() should return feature flow", fields{"", nil, branches}, args{branches[uint8(TypeFeature)].Name()}, &Branch{TypeFeature, branches[uint8(TypeFeature)].Name()}},
		{"GetBranchByName() should return release flow", fields{"", nil, branches}, args{branches[uint8(TypeRelease)].Name()}, &Branch{TypeRelease, branches[uint8(TypeRelease)].Name()}},
		{"GetBranchByName() should return support flow", fields{"", nil, branches}, args{branches[uint8(TypeSupport)].Name()}, &Branch{TypeSupport, branches[uint8(TypeSupport)].Name()}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Flow{
				BranchNamingFormat: tt.fields.BranchNamingFormat,
				BranchNamingRule:   tt.fields.BranchNamingRule,
				Branches:           tt.fields.Branches,
			}
			if got := this.GetBranchByName(tt.args.name); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Flow.GetBranchByName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFlow_Extract(t *testing.T) {
	type fields struct {
		BranchNamingFormat string
		BranchNamingRule   *regexp.Regexp
		Branches           map[uint8]*Branch
	}
	type args struct {
		messageOrBranchName string
	}
	tests := []struct {
		name               string
		fields             fields
		args               args
		wantOk             bool
		wantFlow           *Branch
		wantBranchName     string
		wantLinkedEntity   string
		wantLinkedEntityId string
		wantMessage        string
		wantErr            bool
	}{
		{
			"should_extract_valid_gitflow/bugfix",
			fields{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), GetDefaultBranches()},
			args{"bugfix/1222899-user-story-that-rocks: Rock it baby"},
			true,
			GetDefaultBranch(TypeBugfix),
			"bugfix/1222899-user-story-that-rocks",
			"1222899-user-story-that-rocks",
			"1222899",
			"Rock it baby",
			false,
		},
		{
			"should_extract_valid_gitflow/hotfix",
			fields{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), GetDefaultBranches()},
			args{"hotfix/1222899-user-story-that-rocks: Rock it baby"},
			true,
			GetDefaultBranch(TypeHotfix),
			"hotfix/1222899-user-story-that-rocks",
			"1222899-user-story-that-rocks",
			"1222899",
			"Rock it baby",
			false,
		},
		{
			"should_extract_valid_gitflow/release",
			fields{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), GetDefaultBranches()},
			args{"release/1222899-user-story-that-rocks: Rock it baby"},
			true,
			GetDefaultBranch(TypeRelease),
			"release/1222899-user-story-that-rocks",
			"1222899-user-story-that-rocks",
			"1222899",
			"Rock it baby",
			false,
		},
		{
			"should_extract_valid_gitflow/support",
			fields{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), GetDefaultBranches()},
			args{"support/1222899-user-story-that-rocks: Rock it baby"},
			true,
			GetDefaultBranch(TypeSupport),
			"support/1222899-user-story-that-rocks",
			"1222899-user-story-that-rocks",
			"1222899",
			"Rock it baby",
			false,
		},
		{
			"should_extract_valid_gitflow/feature",
			fields{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), GetDefaultBranches()},
			args{"feature/1222899-user-story-that-rocks: Rock it baby"},
			true,
			GetDefaultBranch(TypeFeature),
			"feature/1222899-user-story-that-rocks",
			"1222899-user-story-that-rocks",
			"1222899",
			"Rock it baby",
			false,
		},
		{
			"should_extract_invalid_gitflow",
			fields{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), GetDefaultBranches()},
			args{"Test commit"},
			false,
			GetDefaultBranch(NoType),
			"",
			"",
			"",
			"Test commit",
			false,
		},
		{
			"should_not_mangle_merge_commits",
			fields{DefaultBranchNamingFormat, regexp.MustCompile(DefaultBranchNamingFormat), GetDefaultBranches()},
			args{"Merge 'feature/test-feature' into develop"},
			true,
			GetDefaultBranch(TypeFeature),
			"feature/test-feature",
			"test-feature",
			"test-feature",
			"Merge 'feature/test-feature' into develop",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Flow{
				BranchNamingFormat: tt.fields.BranchNamingFormat,
				BranchNamingRule:   tt.fields.BranchNamingRule,
				Branches:           tt.fields.Branches,
			}
			gotOk, gotFlow, gotBranchName, gotLinkedEntity, gotLinkedEntityId, gotMessage, err := this.Extract(tt.args.messageOrBranchName)
			if (err != nil) != tt.wantErr {
				t.Errorf("Flow.Extract() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotOk != tt.wantOk {
				t.Errorf("Flow.Extract() gotOk = %v, want %v", gotOk, tt.wantOk)
			} else {
				t.Logf("Flow.Extract() gotOk = %v", gotOk)
			}
			if !reflect.DeepEqual(gotFlow, tt.wantFlow) {
				t.Errorf("Flow.Extract() gotFlow = %v, want %v", gotFlow, tt.wantFlow)
			} else {
				t.Logf("Flow.Extract() gotFlow = %v", gotFlow)
			}
			if gotBranchName != tt.wantBranchName {
				t.Errorf("Flow.Extract() gotBranchName = %v, want %v", gotBranchName, tt.wantBranchName)
			} else {
				t.Logf("Flow.Extract() gotBranchName = %v", gotBranchName)
			}
			if gotLinkedEntity != tt.wantLinkedEntity {
				t.Errorf("Flow.Extract() gotLinkedEntity = %v, want %v", gotLinkedEntity, tt.wantLinkedEntity)
			} else {
				t.Logf("Flow.Extract() gotLinkedEntity = %v", gotLinkedEntity)
			}
			if gotLinkedEntityId != tt.wantLinkedEntityId {
				t.Errorf("Flow.Extract() gotLinkedEntityId = %v, want %v", gotLinkedEntityId, tt.wantLinkedEntityId)
			} else {
				t.Logf("Flow.Extract() gotLinkedEntityId = %v", gotLinkedEntityId)
			}
			if gotMessage != tt.wantMessage {
				t.Errorf("Flow.Extract() gotMessage = %v, want %v", gotMessage, tt.wantMessage)
			} else {
				t.Logf("Flow.Extract() gotMessage = %v", gotMessage)
			}
		})
	}
}
