package gitflow

// The id of the entry type
type Type uint8

// Possible entry type ids
const (
	NoType      Type = iota
	TypeBugfix         = iota
	TypeHotfix         = iota
	TypeFeature        = iota
	TypeSupport        = iota
	TypeRelease        = iota
)

// Stringify the id
func (this Type) String() string {
	return GetDefaultBranchName(this)
}

func GetDefaultBranchName(t Type) string {
	branches := GetDefaultBranches()
	return branches[uint8(t)].Name()
}
