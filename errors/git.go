package errors

import (
	"errors"
	"fmt"
	"os"

	"bitbucket.org/DarkBoss/go-release-notes/git"
)

// New constructs a new error from supplied format string
// and arguments
func New(format string, args ...interface{}) error {
	e := errors.New(fmt.Sprintf(format, args...))
	return e
}

// Git constructs a new error from a supplied commit and error
func Git(c *git.Commit, error string) error {
	return New("[%8.8s] %s", c.Hash, error)
}

// Print writes the error to stderr in a common format
func Print(e error) error {
	fmt.Fprintf(os.Stderr, "[error] %s\n", e.Error())
	return e
}
