package resources

import "github.com/gobuffalo/packr/v2"

var box *packr.Box

func GetBox() *packr.Box {
	return box
}

func Get(name string) ([]byte, error) {
	return box.Find(name)
}

func GetString(name string) (string, error) {
	return box.FindString(name)
}

func init() {
	box = packr.New("resources", "./../resources/embed")
}