var graphOptions = {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }
};

function App() {
    this.contexts = {};
    this.graphs = {};
    this.graphOptions = graphOptions;
    this.graphData = {};
}

App.prototype.createGraph = function (type, name, data) {
    try {
        this.contexts[name] = document.getElementById("release-graph-" + name).getContext('2d');
        this.graphData[name] = {type: type, data: data, options: Object.create(this.graphOptions)};
        this.graphs[name] = new Chart(this.contexts[name], this.graphData[name]);
    } catch (ex) {
        alert("Graph generation failed for " + name + ": " + ex.message);
    }
    return this.graphs[name];
};

App.prototype.toggleClass = function (selector, className) {
    $(selector).each(function (id, elem) {
        var el = $(elem);
        var fn = (el.hasClass(className) ? el.removeClass : el.addClass).bind(el);
        fn(className);
    });
};

App.prototype.togglePanel = function (selector) {
    this.toggleClass(selector, 'collapsed');
};