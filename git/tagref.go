package git

import (
	"fmt"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"reflect"
)

type LightweightTag = plumbing.Reference
type AnnotatedTag = object.Tag

type TagRef struct {
	Name       string
	Hash       plumbing.Hash
	TargetHash plumbing.Hash
	Message    string
	Author     string
	Date       string
}

func NewTagRef(v interface{}) *TagRef {
	rt := reflect.TypeOf(v)
	ref := &TagRef{}
	switch rt {
	case reflect.TypeOf(&LightweightTag{}):
		t := v.(*LightweightTag)
		hash := t.Hash()
		ref.Hash = hash
		ref.TargetHash = hash
		ref.Name = t.Name().Short()
	case reflect.TypeOf(&AnnotatedTag{}):
		t := v.(*AnnotatedTag)
		ref.TargetHash = t.Target
		ref.Message = t.Message
		ref.Hash = t.Hash
		ref.Name = t.Name
		ref.Author = fmt.Sprintf("%s <%s>", t.Tagger.Name, t.Tagger.Email)
		ref.Date = t.Tagger.When.String()
	default:
		panic("Dont know what to do with tags of type: " + rt.String())
	}
	return ref
}
