package git

import (
	"bitbucket.org/DarkBoss/go-release-notes/version"
	"errors"
	"fmt"
	"io"
	"reflect"
	"sort"
	"strings"
)

type SortDir int

const (
	SortAsc  SortDir = -1
	SortDesc         = 1
)

func AllSortDirs() map[int]string {
	return map[int]string{
		int(SortAsc):  "ascending",
		int(SortDesc): "descending",
	}
}

func SortDirName(d SortDir) string {
	return AllSortDirs()[int(d)]
}

func (this SortDir) String() string {
	return SortDirName(this)
}

type Sorter interface {
	Compare(l, r interface{}) int
	Sort(data interface{}, flags SortFlags)
}

type TagRefSorter struct {
	Sorter
	flags SortFlags
	dir   SortDir
}

func NewTagRefSorter() *TagRefSorter {
	ret := &TagRefSorter{
		nil,
		0,
		0,
	}
	ret.SetFlags(DefaultSortFlags)
	return ret
}

func (this *TagRefSorter) Flags() SortFlags {
	return this.flags
}

func (this *TagRefSorter) Dir() SortDir {
	return this.dir
}

func (this *TagRefSorter) SetFlags(flags SortFlags) {
	this.flags = flags
	this.dir = SortAsc
	if (this.flags & SortReverse) != 0 {
		this.dir = SortDesc
	}
}

func (this *TagRefSorter) Compare(l, r interface{}) int {
	lTagRef, rTagRef := l.(*TagRef), r.(*TagRef)
	cmp := 0
	if this.flags.Is(SortByDate) {
		cmp = compTagsByDate(lTagRef, rTagRef)
	} else if this.flags.Is(SortByName) {
		cmp = compTagsByName(lTagRef, rTagRef)
	} else if this.flags.Is(SortByHash) {
		cmp = compTagsByHash(lTagRef, rTagRef)
	}
	return cmp
}

func (this *TagRefSorter) GetTagRefs(data interface{}) (*TagRefList, error) {
	var tagRefs *TagRefList
	var rt = reflect.ValueOf(data)
	switch rt.Kind() {
	case reflect.Slice:
		tagRefs = NewTagRefList(data.([]*TagRef)...)
	default:
		switch data.(type) {
		case *TagRefList:
			tagRefs = data.(*TagRefList)
		}
	}
	if tagRefs == nil {
		return nil, errors.New(fmt.Sprintf("unknown tag list type: %s", rt.Kind().String()))
	}
	return tagRefs, nil
}

func (this *TagRefSorter) Sort(data interface{}, flags SortFlags) error {
	tagRefs, err := this.GetTagRefs(data)
	if err != nil {
		return err
	}
	this.SetFlags(flags)
	sort.SliceStable(tagRefs.tags, func(i, j int) bool {
		l := tagRefs.Get(i)
		r := tagRefs.Get(j)
		cmp := this.Compare(l, r)
		if this.dir < 0 {
			return cmp < 0
		}
		return cmp > 0
	})
	return nil
}

func dumpTags(w io.Writer, prefix, fieldName string, tags []*TagRef) {
	for i, t := range tags {
		rv := reflect.Indirect(reflect.ValueOf(t))
		_, _ = fmt.Fprintf(w, "%s[%d]: %s\n", prefix, i, rv.FieldByName(fieldName).String())
	}
}

func SortOperatorSymbols() map[int]string {
	return map[int]string{
		-1: "<",
		0:  "=",
		1:  ">",
	}
}

func SortOperatorSymbol(cmp int) string {
	switch {
	case cmp < 0:
		cmp = -1
	case cmp > 0:
		cmp = 1
	}
	return SortOperatorSymbols()[cmp]
}

func compTagsByDate(l, r *TagRef) int {
	res := strings.Compare(l.Date, r.Date)
	return res
}

func compTagsByName(l, r *TagRef) int {
	cmp := version.NewComparator(version.DefaultPartWidth, version.DefaultPartCount)
	lText := l.Name
	rText := r.Name
	return cmp(lText, rText)
}

func compTagsByMessage(l, r *TagRef) int {
	cmp := strings.Compare(l.Message, r.Message)
	return cmp
}

func compTagsByHash(l, r *TagRef) int {
	cmp := strings.Compare(l.Hash.String(), r.Hash.String())
	return cmp
}
