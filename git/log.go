package git

import (
	"errors"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

// Reference represents a git reference, tree-ish or hash
type Reference = plumbing.Reference

// Hash represents a git hash
type Hash = plumbing.Hash

// Commit represents a git commit
type Commit = object.Commit

// CommitList represents a git commit list
type CommitList = []*Commit

// CommitIter represents a git commit iterator
type CommitIter = object.CommitIter

type LogOptions = git.LogOptions
type LogOrder = git.LogOrder

// Log represents the git log
type Log struct {
	repo *Repository
}

// NewLog constructs a new Log object. It returns an error
// if the supplied repository is invalid.
func NewLog(repo *Repository) (ret *Log, err error) {
	if repo == nil {
		return nil, errors.New("Invalid repository")
	}
	return &Log{repo}, nil
}

// Find a commit amongst the log commits
func (this *Log) Find(opts *git.LogOptions) (iter CommitIter, err error) {
	// List git-log
	iter, err = this.repo.Log(opts)
	if err != nil {
		return nil, err
	}
	return iter, nil
}

// All returns all logged commits
func (this *Log) All() (CommitIter, error) {
	hash, err := this.repo.HeadHash()
	if err != nil {
		return nil, err
	}
	return this.Find(&git.LogOptions{From: hash})
}

func (this *Log) FindUntil(hash plumbing.Hash, includeNeedle bool) (ret []*Commit, err error) {
	var errStop = errors.New("stop")
	var commits CommitIter
	ret = []*Commit{}
	if commits, err = this.repo.Log(&LogOptions{Order: git.LogOrderCommitterTime}); err != nil {
		return ret, err
	}
	err = commits.ForEach(func(c *Commit) error {
		if !hash.IsZero() && c.Hash.String() == hash.String() {
			if includeNeedle {
				ret = append(ret, c)
			}
			return errStop
		}
		ret = append(ret, c)
		return nil
	})
	if err == errStop {
		err = nil
	}
	return ret, err
}
