package git

import (
	"gopkg.in/src-d/go-git.v4/plumbing"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"
)

var wantedAllFlags = map[int]string{
	int(NoSort):        "none",
	int(SortByDate):    "by_date",
	int(SortByName):    "by_name",
	int(SortByMessage): "by_message",
	int(SortByHash):    "by_hash",
	int(SortReverse):   "reverse",
}

func TestAllSortFlags(t *testing.T) {
	tests := []struct {
		name string
		want map[int]string
	}{
		{"should have all flags", wantedAllFlags},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AllSortFlagNames(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AllSortFlags() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSortFlag_String(t *testing.T) {
	tests := []struct {
		name string
		this SortFlag
		want string
	}{
		{"NoSort should be printable", NoSort, wantedAllFlags[int(NoSort)]},
		{"SortByDate should be printable", SortByDate, wantedAllFlags[int(SortByDate)]},
		{"SortByName should be printable", SortByName, wantedAllFlags[int(SortByName)]},
		{"SortReverse should be printable", SortReverse, wantedAllFlags[int(SortReverse)]},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.String(); got != tt.want {
				t.Errorf("SortFlag.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReduceSortFlags(t *testing.T) {
	type args struct {
		flags []SortFlag
	}
	tests := []struct {
		name string
		args args
		want SortFlags
	}{
		{"reduce multiple flags", args{[]SortFlag{SortReverse, SortByDate}}, SortFlags(SortReverse | SortByDate)},
		{"reduce no flags should yield default flags", args{[]SortFlag{}}, DefaultSortFlags},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ReduceSortFlags(tt.args.flags...); got != tt.want {
				t.Errorf("ReduceSortFlags() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSortFlags_String(t *testing.T) {
	t.Run("multiple flags should be joined by a pipe", func(t *testing.T) {
		this := SortFlags(SortByDate | SortReverse)
		got := this.String()
		wanted := wantedAllFlags[int(SortByDate)]
		if pos := strings.Index(got, wanted); pos == -1 {
			t.Errorf("SortFlags.String() = %s, missing %s", got, wanted)
		} else {
			wanted = wantedAllFlags[int(SortByDate)]
			if pos := strings.Index(got, wanted); pos == -1 {
				t.Errorf("SortFlags.String() = %s, missing %s", got, wanted)
			} else {
				t.Logf("SortFlags.String() = %s", got)
			}
		}
	})
}

func Test_compTagsByDate(t *testing.T) {
	type args struct {
		l       *TagRef
		r       *TagRef
	}
	dayOne := time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)
	dayTwo := time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)
	tests := []struct {
		name string
		args args
		want int
	}{
		{"should be correctly ordered (date)", args{&TagRef{Date: dayOne.String()}, &TagRef{Date: dayTwo.String()}}, -1},
		{"should NOT be correctly ordered (date)", args{&TagRef{Date: dayTwo.String()}, &TagRef{Date: dayOne.String()}}, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := compTagsByDate(tt.args.l, tt.args.r); got != tt.want {
				t.Errorf("compTagsByDate() %s < %s => %d, want %d", tt.args.l.Date, tt.args.r.Date, got, tt.want)
			} else {
				t.Logf("compTagsByDate() %s < %s => %d", tt.args.l.Date, tt.args.r.Date, got)
			}
		})
	}
}

func Test_compTagsByName(t *testing.T) {
	type args struct {
		l       *TagRef
		r       *TagRef
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"should be correctly ordered (name)", args{&TagRef{Name: "1.0.0.0"}, &TagRef{Name: "1.0.0.1"}}, -1},
		{"should NOT be correctly ordered (name)", args{&TagRef{Name: "1.0.0.1"}, &TagRef{Name: "1.0.0.0"}}, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := compTagsByName(tt.args.l, tt.args.r); got != tt.want {
				t.Errorf("compTagsByName(%s, %s) = %v, want %v", tt.args.l.Name, tt.args.r.Name, got, tt.want)
			} else {
				op := func() string {
					switch {
					case got < 0:
						return "<"
					case got == 0:
						return "="
					case got > 0:
						return ">"
					}
					return ""
				}()
				t.Logf("compTagsByName() %s %s %s", tt.args.l.Name, op, tt.args.r.Name)
			}
		})
	}
}

var dayOne = time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)
var dayTwo = time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)
var dayThree = time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)

var wantedTagOrder = [][]string{
	{
		dayThree.String(),
		dayTwo.String(),
		dayOne.String(),
	},
	{
		"1.0.2.0",
		"1.0.0.20",
		"1.0.0.2",
		"0.0.2.0",
	},
}

func TestTagRefList_Sort(t *testing.T) {
	type fields struct {
		tags []*TagRef
	}
	type args struct {
		flags []SortFlag
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		fieldName   string
		wantedOrder []string
		reducer     func(l *TagRefList) []string
	}{
		{"should sort by date (DESC)", fields{[]*TagRef{
			{Date: dayTwo.String()},
			{Date: dayThree.String()},
			{Date: dayOne.String()},
		}}, args{[]SortFlag{SortByDate, SortReverse}}, "Date", []string{
			dayThree.String(),
			dayTwo.String(),
			dayOne.String(),
		}, func(l *TagRefList) []string { return l.AllDates() }},
		{"should sort version numbers (DESC)", fields{[]*TagRef{
			{Name: "0.0.2.0"},
			{Name: "1.0.2.0"},
			{Name: "1.10.0.20"},
			{Name: "1.10.0.2"},
			{Name: "1.10.0.19"},
			{Name: "1.0.20.0"},
		}}, args{[]SortFlag{SortByName, SortReverse}}, "Name", []string{
			"1.10.0.20",
			"1.10.0.19",
			"1.10.0.2",
			"1.0.20.0",
			"1.0.2.0",
			"0.0.2.0",
		}, func(l *TagRefList) []string { return l.AllNames() }},
		{"should sort version numbers with letters", fields{[]*TagRef{
			{Name: "0.0.2.0"},
			{Name: "1.0.2.0-hotfix-1"},
			{Name: "1.0.2.0-hotfix-2"},
			{Name: "1.0.2.0"},
		}}, args{[]SortFlag{SortByName, SortReverse}}, "Name", []string{
			"1.0.2.0-hotfix-2",
			"1.0.2.0-hotfix-1",
			"1.0.2.0",
			"0.0.2.0",
		}, func(l *TagRefList) []string { return l.AllNames() }},
		{"should sort realistic project tags by reverse name", fields{[]*TagRef{
			{Name: "av-noa-abs-1.10.0.35"},
			{Name: "av-noa-abs-1.10.0.34-xldeploy-4"},
			{Name: "av-noa-abs-1.10.0.34-xldeploy-3"},
			{Name: "av-noa-abs-1.10.0.34-xldeploy-2"},
			{Name: "av-noa-abs-1.10.0.34-xldeploy-1"},
			{Name: "av-noa-abs-1.10.0.34"},
			{Name: "av-noa-abs-1.10.0.15-xldeploy-10"},
		}}, args{[]SortFlag{SortByName, SortReverse}}, "Name", []string{
			"av-noa-abs-1.10.0.35",
			"av-noa-abs-1.10.0.34-xldeploy-4",
			"av-noa-abs-1.10.0.34-xldeploy-3",
			"av-noa-abs-1.10.0.34-xldeploy-2",
			"av-noa-abs-1.10.0.34-xldeploy-1",
			"av-noa-abs-1.10.0.34",
			"av-noa-abs-1.10.0.15-xldeploy-10",
		}, func(l *TagRefList) []string { return l.AllNames() }},
		{"should sort realistic project tags by reverse name", fields{[]*TagRef{
			{Name: "dev-1.10.0.35"},
			{Name: "dev-1.10.0.34-xldeploy-4"},
			{Name: "dev-1.10.0.34-xldeploy-3"},
			{Name: "dev-1.10.0.34-xldeploy-2"},
			{Name: "dev-1.10.0.34-xldeploy-1"},
			{Name: "dev-1.10.0.34"},
			{Name: "dev-1.10.0.15-xldeploy-10"},
			{Name: "master-1.10.0.35"},
			{Name: "master-1.10.0.34-xldeploy-4"},
			{Name: "master-1.10.0.34-xldeploy-3"},
			{Name: "master-1.10.0.34-xldeploy-2"},
			{Name: "master-1.10.0.34-xldeploy-1"},
			{Name: "master-1.10.0.34"},
			{Name: "master-1.10.0.15-xldeploy-10"},
		}}, args{[]SortFlag{SortByName, SortReverse}}, "Name", []string{
			"dev-1.10.0.35",
			"master-1.10.0.35",

			"dev-1.10.0.34-xldeploy-4",
			"master-1.10.0.34-xldeploy-4",

			"dev-1.10.0.34-xldeploy-3",
			"master-1.10.0.34-xldeploy-3",

			"dev-1.10.0.34-xldeploy-2",
			"master-1.10.0.34-xldeploy-2",

			"dev-1.10.0.34-xldeploy-1",
			"master-1.10.0.34-xldeploy-1",

			"dev-1.10.0.34",
			"master-1.10.0.34",

			"dev-1.10.0.15-xldeploy-10",
			"master-1.10.0.15-xldeploy-10",
		}, func(l *TagRefList) []string { return l.AllNames() }},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &TagRefList{
				tags: tt.fields.tags,
			}
			dumpTags(os.Stderr, "TagRefListTest.Sort() start ", tt.fieldName, this.tags)
			if err := this.Sort(tt.args.flags...); err != nil {
				t.Errorf("TagRefList.Sort() error %s", err.Error())
			} else {
				dumpTags(os.Stderr, "TagRefListTest.Sort() stop ", tt.fieldName, this.tags)
				got := tt.reducer(this)
				if !reflect.DeepEqual(tt.wantedOrder, got) {
					t.Errorf("TagRefList.Sort() order doesn't match, got %v, wanted %v", got, tt.wantedOrder)
				} else {
					t.Logf("TagRefList.Sort() final order %v", got)
				}
			}
		})
	}
}

func TestTagRefList_SortCustom(t *testing.T) {
	type fields struct {
		tags []*TagRef
	}
	type args struct {
		fn func(i, j int) bool
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &TagRefList{
				tags: tt.fields.tags,
			}
			this.SortCustom(tt.args.fn)
		})
	}
}

func TestTagRefList_Flatten(t *testing.T) {
	type fields struct {
		tags []*TagRef
	}
	type args struct {
		fn func(*TagRef) string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []string
	}{
		{"should flatten tag names", fields{[]*TagRef{{Name: "a"}, {Name: "b"}, {Name: "c"}}}, args{func(t *TagRef) string { return t.Name }}, []string{"a", "b", "c"}},
		{"should flatten tag hashes", fields{[]*TagRef{{Hash: plumbing.NewHash("a2fa000000000000000000000000000000000000")}, {Hash: plumbing.NewHash("a2fa000000000000000000000000000000000001")}, {Hash: plumbing.NewHash("a2fa000000000000000000000000000000000002")}}}, args{func(t *TagRef) string { return t.Hash.String() }}, []string{"a2fa000000000000000000000000000000000000", "a2fa000000000000000000000000000000000001", "a2fa000000000000000000000000000000000002"}},
		{"should flatten tag messages", fields{[]*TagRef{{Message: "a"}, {Message: "b"}, {Message: "c"}}}, args{func(t *TagRef) string { return t.Message }}, []string{"a", "b", "c"}},
		{"should flatten tag dates", fields{[]*TagRef{{Date: "a"}, {Date: "b"}, {Date: "c"}}}, args{func(t *TagRef) string { return t.Date }}, []string{"a", "b", "c"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &TagRefList{
				tags: tt.fields.tags,
			}
			if got := this.Flatten(tt.args.fn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TagRefList.Flatten() = %v, want %v", got, tt.want)
			}
		})
	}
}
