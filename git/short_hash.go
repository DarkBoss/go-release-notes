package git

import (
	"fmt"
)

// ShortHashLength represents the number of characters to be kept in the short form.
const ShortHashLength = 8

// ShortHash transforms a git hash into it's short form.
func ShortHash(h string) string {
	lenFmt := fmt.Sprintf("%d.%d", ShortHashLength, ShortHashLength)
	return fmt.Sprintf("%"+lenFmt+"s", h)
}
