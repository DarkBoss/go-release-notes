package git

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"

	"gopkg.in/src-d/go-billy.v4/memfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/storage/memory"
)

func createRepository(files StrMap) (*git.Repository, error) {
	store := memory.NewStorage()
	wtree := memfs.New()
	repo, err := git.Init(store, wtree)
	if err != nil {
		return nil, err
	}
	for iFileName, iFileContent := range files {
		if oFile, err := wtree.Create(iFileName); err != nil {
			return nil, err
		} else {
			if _, err = oFile.Write([]byte(iFileContent)); err != nil {
				return nil, err
			}
		}
	}
	return repo, nil
}

func repositoryMust(r *git.Repository, e error) func(t *testing.T) *git.Repository {
	return func(t *testing.T) *git.Repository {
		if e != nil {
			t.Fatalf("failed to open virtual repository: %s", e)
		}
		return r
	}
}

type StrMap = map[string]string

type DummyFileInfo struct {
	name  string
	size  int64
	isDir bool
}

func (this *DummyFileInfo) Name() string       { return this.name }
func (this *DummyFileInfo) Size() int64        { return this.size }
func (this *DummyFileInfo) Mode() os.FileMode  { return os.FileMode(0) }
func (this *DummyFileInfo) ModTime() time.Time { return time.Now() }
func (this *DummyFileInfo) IsDir() bool        { return this.isDir }
func (this *DummyFileInfo) Sys() interface{}   { return nil }

func FileInfoEquals(r, l os.FileInfo) bool {
	return r.Name() == l.Name() && r.IsDir() == l.IsDir() && r.Size() == l.Size()
}

func newDummyFileInfos(name string, size int64, isDir bool) *DummyFileInfo {
	return &DummyFileInfo{name, size, isDir}
}

func checkFileInfos(got, want map[string]os.FileInfo) (bool, error) {
	if len(got) != len(want) {
		return false, nil
	}
	for wantKey, wantInfos := range want {
		if val, ok := got[wantKey]; !ok {
			return false, fmt.Errorf("missing key '%s', in %v", wantKey, got)
		} else if !FileInfoEquals(val, wantInfos) {
			return false, fmt.Errorf("%s = %v, want %v", wantKey, val, wantInfos)
		}
	}
	return true, nil
}

func TestRepository_Filter(t *testing.T) {
	type fields struct {
		Repository *git.Repository
		Name       string
		Path       string
	}
	type args struct {
		path string
		fn   RepositoryFilterPredicate
	}
	emptyRepo := repositoryMust(createRepository(StrMap{}))(t)
	nonEmptyRepo := repositoryMust(createRepository(StrMap{
		"test-file-1": "blah blah blahg",
	}))(t)
	emptyResults := map[string]os.FileInfo{}
	everything := func(path string, info os.FileInfo) bool {
		println("filter: " + path)
		return true
	}
	nothing := func(path string, info os.FileInfo) bool { return false }
	nonEmptyResult := map[string]os.FileInfo{
		(string(os.PathSeparator) + "test-file-1"): newDummyFileInfos("test-file-1", 15, false),
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]os.FileInfo
		wantErr bool
	}{
		{
			"filtering empty repository should yield empty results",
			fields{emptyRepo, "", ""},
			args{string(os.PathSeparator), everything},
			emptyResults,
			false,
		},
		{
			"filtering repository containing file should yield the file if predicate returns true",
			fields{nonEmptyRepo, "non-empty-repo", "."},
			args{string(os.PathSeparator), everything},
			nonEmptyResult,
			false,
		},
		{
			"filtering repository containing file should yield no file if predicate returns false",
			fields{nonEmptyRepo, "non-empty-repo", "."},
			args{string(os.PathSeparator), nothing},
			emptyResults,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Repository{
				Repository: tt.fields.Repository,
				name:       tt.fields.Name,
				path:       tt.fields.Path,
			}
			got, err := this.Filter(tt.args.path, tt.args.fn)
			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.Filter('%s') = %v, want %v\n\t-> error = %v, wantErr %v", tt.args.path, got, tt.want, err, tt.wantErr)
				return
			} else if ok, err := checkFileInfos(got, tt.want); !ok != tt.wantErr {
				t.Errorf("Repository.Filter('%s') = %v, want %v", tt.args.path, got, tt.want)
			} else if (err != nil) != tt.wantErr {
				t.Errorf("Repository.Filter('%s'): %v (wantErr: %t)", tt.args.path, err, tt.wantErr)
			} else {
				t.Logf("Repository.Filter('%s') = %v", tt.args.path, got)
			}
		})
	}
}

func TestOpenRepository(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    *Repository
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := OpenRepository(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpenRepository() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OpenRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRepository_HeadHash(t *testing.T) {
	type fields struct {
		Repository *git.Repository
		Name       string
		Path       string
	}
	tests := []struct {
		name    string
		fields  fields
		want    plumbing.Hash
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Repository{
				Repository: tt.fields.Repository,
				name:       tt.fields.Name,
				path:       tt.fields.Path,
			}
			got, err := this.HeadHash()
			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.HeadHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Repository.HeadHash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRepository_ReadFile(t *testing.T) {
	type fields struct {
		Repository *git.Repository
		Name       string
		Path       string
	}
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Repository{
				Repository: tt.fields.Repository,
				name:       tt.fields.Name,
				path:       tt.fields.Path,
			}
			got, err := this.ReadFile(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.ReadFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Repository.ReadFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

type TagMap = map[string][]string
type BranchMap = map[string]TagMap

func createTestRepository(name string, branches BranchMap) (*Repository, []*TagRef, error) {
	wantedTags := []*TagRef{}
	tagAuthor := &object.Signature{
		Name:  "Test",
		Email: "tester@testing.com",
		When:  time.Now(),
	}
	tagOptions := &git.CreateTagOptions{Tagger: tagAuthor}
	repo, err := InitEmptyRepository(name)
	if err != nil {
		return nil, nil, err
	}
	wt, err := repo.Worktree()
	if err != nil {
		return nil, nil, err
	}
	//if err = repo.CreateBranch(&config.Branch{Name: "master"}); err != nil {
	//	return nil, nil, err
	//}
	//if err = repo.CreateBranch(&config.Branch{Name: "develop"}); err != nil {
	//	return nil, nil, err
	//}
	checkoutBranch := func(n string, from plumbing.Hash) (*plumbing.Reference, error) {
		var refName = plumbing.NewBranchReferenceName(n)
		iter, err := repo.Branches()
		var found *plumbing.Reference
		if err != nil {
			return nil, err
		}
		_ = iter.ForEach(func(ref *plumbing.Reference) error {
			if ref.Name() == refName {
				found = ref
				return errors.New("")
			}
			return nil
		})
		if found == nil {
			if err = wt.Checkout(&git.CheckoutOptions{Branch: refName, Create: true, Force: true, Hash: from}); err != nil {
				return nil, err
			}
		}
		return found, nil
	}

	if hash, err := wt.Commit("initial commit", &git.CommitOptions{Author: tagAuthor, All: true}); err != nil {
		return nil, nil, err
	} else {
		if _, err = checkoutBranch("develop", hash); err != nil {
			return nil, nil, err
		}
	}

	head, err := repo.Head()
	if err != nil {
		return nil, nil, err
	}

	addFile := func(branchName, name string) (plumbing.Hash, error) {
		if _, err := checkoutBranch(branchName, head.Hash()); err != nil {
			return plumbing.Hash{}, err
		}
		if _, err := wt.Filesystem.Create(name); err != nil {
			return plumbing.Hash{}, err
		}
		hash, err := wt.Add(name)
		if err != nil {
			return plumbing.Hash{}, err
		}
		return hash, nil
	}

	createTag := func(name string, msg string, from plumbing.Hash) (plumbing.Hash, error) {
		opts := &git.CreateTagOptions{}
		*opts = *tagOptions
		opts.Message = msg
		if tag1, err := repo.CreateTag(name, from, opts); err == nil {
			//log.Printf("tag '%s' -> %8.8s\tfrom: %8.8s", name, tag1.Hash(), from.String())
			wantedTags = append(wantedTags, &TagRef{
				tag1.Name().Short(),
				tag1.Hash(),
				tag1.Hash(),
				msg,
				fmt.Sprintf("%s <%s>", tagAuthor.Name, tagAuthor.Email),
				tagAuthor.When.String(),
			})
			return tag1.Hash(), nil
		} else {
			return plumbing.Hash{}, err
		}
	}
	var hash plumbing.Hash
	for branch, tags := range branches {
		for tagName, tag := range tags {
			for _, f := range tag {
				if hash, err = addFile(branch, f); err != nil {
					return nil, nil, err
				}
			}
			if hash, err = wt.Commit("Add files", &git.CommitOptions{Author: tagAuthor}); err != nil {
				return nil, nil, err
			}
			if _, err = createTag(tagName, tagName, hash); err != nil {
				return nil, nil, err
			} else {
				//log.Printf("Created tag %s on %s", tagName, branch)
			}
		}
	}
	return repo, wantedTags, nil
}

type TagTestCommit = []string
type TagTestCommitMap = map[string]TagTestCommit
type TagTestBranch = map[string]TagTestCommitMap

var tags = []TagTestBranch{
	{
		"develop": {
			"1.0.0.2":  []string{"file-1"},
			"1.0.0.20": []string{"file-2"},
			"1.0.2.0":  []string{"file-3"},
			"0.0.2.0":  []string{"file-4"},
		},
	},
	{
		"develop": {
			"av-noa-abs-1.10.0.15-xldeploy-10": []string{"file-1"},
			"av-noa-abs-1.10.0.34-xldeploy-1":  []string{"file-2"},
			"av-noa-abs-1.10.0.34-xldeploy-2":  []string{"file-3"},
			"av-noa-abs-1.10.0.34-xldeploy-3":  []string{"file-4"},
			"av-noa-abs-1.10.0.34-xldeploy-4":  []string{"file-5"},
			"av-noa-abs-1.10.0.34":             []string{"file-6"},
			"av-noa-abs-1.10.0.35":             []string{"file-7"},
		},
	},
	{
		"develop": {
			"dev-1.10.0.15-xldeploy-10": []string{"file-1"},
			"dev-1.10.0.34-xldeploy-1":  []string{"file-2"},
			"dev-1.10.0.34-xldeploy-2":  []string{"file-3"},
			"dev-1.10.0.34-xldeploy-3":  []string{"file-4"},
			"dev-1.10.0.34-xldeploy-4":  []string{"file-5"},
			"dev-1.10.0.34":             []string{"file-6"},
			"dev-1.10.0.35":             []string{"file-7"},
		},
		"master": {
			"master-1.10.0.15-xldeploy-10": []string{"file-1"},
			"master-1.10.0.34-xldeploy-1":  []string{"file-2"},
			"master-1.10.0.34-xldeploy-2":  []string{"file-3"},
			"master-1.10.0.34-xldeploy-3":  []string{"file-4"},
			"master-1.10.0.34-xldeploy-4":  []string{"file-5"},
			"master-1.10.0.34":             []string{"file-6"},
			"master-1.10.0.35":             []string{"file-7"},
		},
	},
}

func TestRepository_FilterTags(t *testing.T) {
	type fields struct {
		Repository *git.Repository
		name       string
		path       string
	}
	type args struct {
		fn    func(*TagRef) bool
	}

	repo1, wantedTags1, err1 := createTestRepository("test-repo-1", tags[0])
	if err1 != nil {
		t.Fatalf("Repository.FilterTags() error %s", err1.Error())
	}

	repo2, wantedTags2, err2 := createTestRepository("test-repo-2", tags[1])
	if err2 != nil {
		t.Fatalf("Repository.FilterTags() error %s", err2.Error())
	}

	repo3, wantedTags3, err3 := createTestRepository("test-repo-3", tags[2])
	if err3 != nil {
		t.Fatalf("Repository.FilterTags() error %s", err3.Error())
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []*TagRef
		wantErr bool
	}{
		{"most recent tags should be listed first", fields{repo1.Repository, repo1.Name(), repo1.Path()}, args{func(t *TagRef) bool { return true }}, wantedTags1, false},
		{"realistic tags should be in proper order", fields{repo2.Repository, repo2.Name(), repo2.Path()}, args{func(t *TagRef) bool { return true }}, wantedTags2, false},
		{"commits on multiple branches should be in proper order", fields{repo3.Repository, repo3.Name(), repo3.Path()}, args{func(t *TagRef) bool { return true }}, wantedTags3, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Repository{
				Repository: tt.fields.Repository,
				name:       tt.fields.name,
				path:       tt.fields.path,
			}
			got, err := this.FilterTags(tt.args.fn)
			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.FilterTags() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			var found *TagRef
			gotOrder := []string{}
			for wantId, wantTag := range tt.want {
				found = got.Find(func(t *TagRef) bool {
					if strings.Compare(wantTag.Hash.String(), t.Hash.String()) == 0 {
						gotOrder = append(gotOrder, t.Message)
						return true
					}
					return false
				})
				if found == nil {
					t.Errorf("Repository.FilterTags() missing tag #%d: %8.8s", wantId, wantTag.Hash.String())
				} else {
					t.Logf("Repository.FilterTags() returned tag #%d: %8.8s - %s", wantId, wantTag.Hash, wantTag.Name)
				}
			}
		})
	}
}
