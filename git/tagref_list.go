package git

import (
	"sort"
	"strings"
)

type MappedTagRefList = map[string]interface{}

type TagRefList struct {
	tags []*TagRef
}

func NewTagRefList(tagRefs ...*TagRef) *TagRefList {
	return &TagRefList{tagRefs}
}

func (this *TagRefList) Sort(flags ...SortFlag) error {
	rflags := ReduceSortFlags(flags...)
	sorter := NewTagRefSorter()
	return sorter.Sort(this, rflags)
}

func (this *TagRefList) Slice() []*TagRef {
	return this.tags
}

func (this *TagRefList) Flatten(fn func(*TagRef) string) []string {
	return *this.Reduce(&[]string{}, func(carry interface{}, item *TagRef) interface{} {
		*carry.(*[]string) = append(*carry.(*[]string), fn(item))
		return carry
	}).(*[]string)
}

func (this *TagRefList) AllMessages() []string {
	return this.Flatten(func(item *TagRef) string {
		return item.Message
	})
}

func (this *TagRefList) AllHashes() []string {
	return this.Flatten(func(item *TagRef) string {
		return item.Hash.String()
	})
}

func (this *TagRefList) AllNames() []string {
	return this.Flatten(func(item *TagRef) string {
		return item.Name
	})
}

func (this *TagRefList) AllDates() []string {
	return this.Flatten(func(item *TagRef) string {
		return item.Date
	})
}

func (this *TagRefList) JoinMessages(sep string) string {
	return strings.Join(this.AllMessages(), sep)
}

func (this *TagRefList) JoinDates(sep string) string {
	return strings.Join(this.AllDates(), sep)
}

func (this *TagRefList) JoinHashes(sep string) string {
	return strings.Join(this.AllHashes(), sep)
}

func (this *TagRefList) JoinNames(sep string) string {
	return strings.Join(this.AllNames(), sep)
}

func (this *TagRefList) Reduce(carry interface{}, fn func(carry interface{}, item *TagRef) interface{}) interface{} {
	for _, t := range this.tags {
		carry = fn(carry, t)
	}
	return carry
}

func (this *TagRefList) Filter(fn func(t *TagRef) bool, limit int) *TagRefList {
	ret := &TagRefList{}
	n := 0
	for _, t := range this.tags {
		if limit > -1 && n >= limit {
			break
		}
		if fn(t) {
			ret.Add(t)
			n++
		}
	}
	return ret
}

func (this *TagRefList) Find(fn func(t *TagRef) bool) *TagRef {
	found := this.Filter(fn, 1)
	if found.Len() > 0 {
		return found.First()
	}
	return nil
}

func (this *TagRefList) Map(fn func(v *TagRef) (bool, string, interface{})) MappedTagRefList {
	var ok bool
	var k string
	var t *TagRef
	var v interface{}
	return this.Reduce(MappedTagRefList{}, func(carry interface{}, item *TagRef) interface{} {
		if ok, k, v = fn(t); ok {
			carry.(MappedTagRefList)[k] = v
		}
		return carry
	}).(MappedTagRefList)
}

func (this *TagRefList) Add(refs ...*TagRef) *TagRefList {
	this.tags = append(this.tags, refs...)
	return this
}

func (this *TagRefList) Get(id int) *TagRef {
	if id < len(this.tags) {
		return this.tags[id]
	}
	return nil
}

func (this *TagRefList) Has(id int) bool {
	return this.Get(id) != nil
}

func (this *TagRefList) Len() int {
	return len(this.tags)
}

func (this *TagRefList) First() *TagRef {
	return this.Get(0)
}

func (this *TagRefList) Last() *TagRef {
	return this.Get(this.Len() - 1)
}

func (this *TagRefList) SubSlice(start int, stop int) *TagRefList {
	return NewTagRefList(this.tags[start:stop]...)
}

func (this *TagRefList) All() []*TagRef {
	return this.Slice()
}
func (this *TagRefList) SortCustom(fn func(i, j int) bool) {
	sort.SliceStable(this.tags, fn)
}
