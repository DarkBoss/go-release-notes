package git

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gopkg.in/src-d/go-billy.v4/memfs"
	"gopkg.in/src-d/go-git.v4/storage/memory"

	"gopkg.in/src-d/go-billy.v4"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

// RepositoryFilterPredicate represents a filtering predicate
// to allow in-repository resources scanning.
type RepositoryFilterPredicate func(string, os.FileInfo) bool

// The Repository struct wraps the original repository
// to gives it a name.
type Repository struct {
	*git.Repository
	name string
	path string
}

func InitEmptyRepository(name string) (*Repository, error) {
	fs := memfs.New()
	return InitRepository(name, fs)
}

func InitRepository(name string, fs billy.Filesystem) (*Repository, error) {
	store := memory.NewStorage()
	if r, err := git.Init(store, fs); err != nil {
		return nil, err
	} else {
		return NewRepository(name, r), nil
	}
}

// NewRepository constructs a new repository with a ;name
// and a repository handle.
func NewRepository(path string, repo *git.Repository) *Repository {
	r := &Repository{repo, "", ""}
	r.SetPath(path)
	return r
}

func (this *Repository) SetName(n string) {
	this.name = n
}

func (this *Repository) SetPath(p string) {
	this.path = p
	this.name = filepath.Base(p)
}

func (this *Repository) Path() string {
	return this.path
}

func (this *Repository) Name() string {
	return this.name
}

// Filter the repository starting at <path> using the predicate <fn>
func (this *Repository) Filter(path string, fn RepositoryFilterPredicate) (map[string]os.FileInfo, error) {
	return this._filter(path, fn, nil)
}

// OpenRepository parses the git repository located at <path>
func OpenRepository(path string) (*Repository, error) {
	repo, err := git.PlainOpen(path)
	if err != nil {
		return nil, fmt.Errorf("%s: failed to open repository: %s", path, err)
	}
	return NewRepository(path, repo), nil
}

// HeadHash return the current branch's head
func (this *Repository) HeadHash() (plumbing.Hash, error) {
	ref, err := this.Head()
	if err != nil {
		return plumbing.Hash{}, err
	}
	return ref.Hash(), nil
}

// ReadFile reads the content of an in-repository file.
func (this *Repository) ReadFile(path string) ([]byte, error) {
	var ret []byte
	var f billy.File
	if tree, err := this.Worktree(); err != nil {
		return ret, err
	} else {
		if f, err = tree.Filesystem.Open(path); err != nil {
			return ret, err
		}
		if ret, err = ioutil.ReadAll(f); err != nil {
			return ret, err
		}
	}
	return ret, nil
}

// GetPathInsideRepo allows to transform a relative resource path
// into a foramt understandable by the git.v4 repository.
// (Mainly adding a PathSeparator prefix)
func (this *Repository) GetPathInsideRepo(path string) string {
	sep := string(os.PathSeparator)
	if strings.Index(path, sep) != 0 {
		path = sep + path
	}
	return path
}

// _filter is the recursive worker implementation of the Filter method
func (this *Repository) _filter(path string, fn RepositoryFilterPredicate, info os.FileInfo) (map[string]os.FileInfo, error) {
	path = this.GetPathInsideRepo(path)
	ret := map[string]os.FileInfo{}
	var dirRet map[string]os.FileInfo
	curPath := path
	if tree, err := this.Worktree(); err != nil {
		return nil, err
	} else {
		if files, err := tree.Filesystem.ReadDir(path); err != nil {
			return nil, err
		} else {
			for _, f := range files {
				curPath = this.GetPathInsideRepo(filepath.Join(path, f.Name()))
				if f.IsDir() {
					if dirRet, err = this._filter(curPath, fn, f); err != nil {
						return nil, err
					}
					for k, v := range dirRet {
						ret[k] = v
					}
				} else {
					if fn(curPath, f) {
						ret[curPath] = f
					}
				}
			}
		}
	}
	return ret, nil
}

func (this *Repository) FilterTags(fn func(*TagRef) bool) (*TagRefList, error) {
	allTagsCache := map[string]*TagRef{}
	allTags := &TagRefList{}
	tagrefs, err := this.Repository.Tags()
	if err != nil {
		return nil, err
	}
	addTag := func(k string, v interface{}) *TagRef {
		allTagsCache[k] = NewTagRef(v)
		return allTagsCache[k]
	}
	// lightweight tags
	if err = tagrefs.ForEach(func(t *LightweightTag) error {
		addTag(t.Name().Short(), t)
		return nil
	}); err != nil {
		return nil, err
	}
	// annotated tags
	if tags, err := this.Repository.TagObjects(); err != nil {
		return nil, err
	} else if err = tags.ForEach(func(t *AnnotatedTag) error {
		addTag(t.Name, t)
		return nil
	}); err != nil {
		return nil, err
	}
	for _, v := range allTagsCache {
		allTags.Add(v)
	}
	return allTags, nil
}
