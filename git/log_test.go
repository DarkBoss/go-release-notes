package git

import (
	"bytes"
	"fmt"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"reflect"
	"sort"
	"testing"
	"time"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

func TestLog_FindUntil(t *testing.T) {
	type fields struct {
		Repository *git.Repository
		name       string
		path       string
	}
	type args struct {
		hash plumbing.Hash
	}
	var wantedCommits []plumbing.Hash
	var err error
	var repo *Repository
	var wt *git.Worktree
	if repo, err = InitEmptyRepository("test"); err != nil {
		t.Errorf("Repository.FindUntil() init error %s", err.Error())
	} else if wt, err = repo.Worktree(); err != nil {
		t.Errorf("Repository.FindUntil() init error %s", err.Error())
	} else if _, err = wt.Commit("initial commit", &git.CommitOptions{All: true, Author: &object.Signature{Name: "test", Email: "tester@test.com", When: time.Now()}}); err != nil {
		t.Errorf("Repository.FindUntil() commit error %s", err.Error())
	}
	if c, err := wt.Commit("wanted commit", &git.CommitOptions{All: true, Author: &object.Signature{Name: "test", Email: "tester@test.com", When: time.Now()}}); err != nil {
		t.Errorf("Repository.FindUntil() commit error %s", err.Error())
	} else {
		wantedCommits = append(wantedCommits, c)
	}
	if c, err := wt.Commit("noise commit 1", &git.CommitOptions{All: true, Author: &object.Signature{Name: "test", Email: "tester@test.com", When: time.Now()}}); err != nil {
		t.Errorf("Repository.FindUntil() commit error %s", err.Error())
	} else {
		wantedCommits = append(wantedCommits, c)
	}
	if c, err := wt.Commit("noise commit 2", &git.CommitOptions{All: true, Author: &object.Signature{Name: "test", Email: "tester@test.com", When: time.Now()}}); err != nil {
		t.Errorf("Repository.FindUntil() commit error %s", err.Error())
	} else {
		wantedCommits = append(wantedCommits, c)
	}
	hashIndex := func(h plumbing.Hash, in []plumbing.Hash) int {
		for i, w := range in {
			if h == w {
				return i
			}
		}
		return -1
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantRet []plumbing.Hash
		wantErr bool
	}{
		{"FindUntil should support long format", fields{repo.Repository, repo.name, repo.path}, args{wantedCommits[0]}, wantedCommits, false},
	}
	flattenCommits := func(l []*Commit) []plumbing.Hash {
		ret := []plumbing.Hash {}
		for _, i := range l {
			ret = append(ret, i.Hash)
		}
		return ret
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Log{
				repo: repo,
			}
			gotRet, err := this.FindUntil(tt.args.hash, true)
			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.FindUntil(%8.8s) error = %v, wantErr %v", tt.args.hash, err, tt.wantErr)
				return
			}
			flatGotRet := flattenCommits(gotRet)
			sort.SliceStable(flatGotRet, func(i, j int) bool {
				return flatGotRet[i].String() < flatGotRet[j].String()
			})
			sort.SliceStable(tt.wantRet, func(i, j int) bool {
				return tt.wantRet[i].String() < tt.wantRet[j].String()
			})
			if !reflect.DeepEqual(flatGotRet, tt.wantRet) {
				t.Errorf("Repository.FindUntil(%8.8s) = %v, want %v", tt.args.hash, flatGotRet, tt.wantRet)
			} else {
				hasError := false
				for _, gotW := range gotRet {
					if hashIndex(gotW.Hash, tt.wantRet) == -1 {
						hasError = true
						t.Errorf("Repository.FindUntil(%8.8s) = commit shouldn't have let through '%8.8s', want %v", tt.args.hash, gotW.String(), wantedCommits)
					} else {
						t.Logf("Repository.FindUntil(%8.8s) got '%8.8s'", tt.args.hash, gotW.Hash.String())
					}
				}
				if !hasError {
					t.Logf("Repository.FindUntil(%8.8s) = %s", tt.args.hash, func() string {
						buf := bytes.NewBufferString("")
						fmt.Fprintf(buf, "%d commits:\n", len(wantedCommits))
						for _, w := range wantedCommits {
							fmt.Fprintf(buf, "\t- %s\n", w.String())
						}
						return buf.String()
					}())
				}
			}
		})
	}
}
