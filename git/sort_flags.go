package git

import "strings"

type SortFlag int

const (
	NoSort        SortFlag = iota
	SortByDate             = 1 << iota
	SortByName             = 1 << iota
	SortByMessage          = 1 << iota
	SortByHash             = 1 << iota
	SortReverse            = 1 << iota

	DefaultSortFlags = SortByName | SortReverse
)

type sortFlagHandler struct {
	Name    string
	Handler func(flags SortFlags, l, r *TagRef) bool
}

var sortFlagHandlers = map[int]sortFlagHandler{
	int(NoSort): {"none", func(flags SortFlags, l, r *TagRef) bool { return true }},
	int(SortByDate): {"by_date", func(flags SortFlags, l, r *TagRef) bool {
		return compTagsByDate(l, r) < 0
	}},
	int(SortByName): {"by_name", func(flags SortFlags, l, r *TagRef) bool {
		return compTagsByName(l, r) < 0

	}},
	int(SortByHash): {"by_hash", func(flags SortFlags, l, r *TagRef) bool {
		return compTagsByHash(l, r) < 0

	}},
	int(SortByMessage): {"by_message", func(flags SortFlags, l, r *TagRef) bool {
		return compTagsByMessage(l, r) < 0

	}},
	int(SortReverse): {"reverse", func(flags SortFlags, l, r *TagRef) bool {
		if flags == SortReverse {
			return strings.Compare(l.Name, r.Name) > 0
		}
		return true
	}},
}

func AllSortFlagNames() map[int]string {
	ret := map[int]string{}
	for k, h := range sortFlagHandlers {
		ret[int(k)] = h.Name
	}
	return ret
}

func GetSortFlagName(f SortFlag) string {
	all := AllSortFlagNames()
	return all[int(f)]
}

func (this SortFlag) String() string {
	return GetSortFlagName(this)
}

type SortFlags int

func ReduceSortFlags(flags ...SortFlag) SortFlags {
	ret := int(DefaultSortFlags)
	if len(flags) > 0 {
		ret = 0
		for _, f := range flags {
			ret = int(ret) | int(f)
		}
	}
	return SortFlags(ret)
}

func (this SortFlags) Is(f ...SortFlag) bool {
	for _, flg := range f {
		if (int(this) & int(flg)) == 0 {
			return false
		}
	}
	return true
}

func (this SortFlags) Set(f SortFlag) {
	this = SortFlags(int(this) | int(f))
}

func (this SortFlags) String() string {
	all := AllSortFlagNames()
	ret := ""
	for val, name := range all {
		if val != int(NoSort) && this.Is(SortFlag(val)) {
			if len(ret) > 0 {
				ret += " | "
			}
			ret += name
		}
	}
	if len(ret) == 0 {
		ret = all[int(NoSort)]
	}
	return ret
}
