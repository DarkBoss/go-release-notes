package version

import "testing"

func TestNormalize(t *testing.T) {
	type args struct {
		s     string
		width int
		parts int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"long version should be ok", args{"1.0.0.0", 3, 4}, "001000000000"},
		{"short version should be ok", args{"1.0", 3, 4}, "001000000000"},
		{"alpha num versions should be ok", args{"1.0.0.0-test", 3, 4}, "001000000000-test"},
		{"version inside sentence should be ok", args{"hello '1.0.0.0-test' ", 3, 4}, "hello '001000000000-test' "},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Normalize(tt.args.s, tt.args.width, tt.args.parts); got != tt.want {
				t.Errorf("Normalize() = %v, want %v", got, tt.want)
			} else {
				t.Logf("Normalize(%s) => %s", tt.args.s, got)
			}
		})
	}
}
