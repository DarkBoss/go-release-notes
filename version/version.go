package version

import (
	"fmt"
	"regexp"
	"strings"
)

// See https://stackoverflow.com/questions/18409373/how-to-compare-two-version-number-strings-in-golang

const DefaultPartWidth = 4
const DefaultPartCount = 4

func Normalize(s string, width, parts int) string {
	rule := regexp.MustCompile(`([\d\.]+)`)
	return rule.ReplaceAllStringFunc(s, func(s string) string {
		strList := strings.Split(s, ".")
		v := ""
		for _, value := range strList {
			v += fmt.Sprintf("%0*s", width, value)
		}
		for i := len(strList); i < parts; i++ {
			v += fmt.Sprintf("%0*s", width, "")
		}
		return v
	})
}

func NewComparator(width, parts int) func(l, r string) int {
	return func(l, r string) int {
		cmp := func(ll, rr string) int {
			return strings.Compare(ll, rr)
		}
		lNorm, rNorm := "", ""
		lRest, rRest := "", ""
		digitsRule := regexp.MustCompile(`([\d\.]+)`)
		wordsRule := regexp.MustCompile(`([^\d\.]+)`)
		if digits := digitsRule.FindStringSubmatch(l); len(digits) > 0 {
			lNorm = digits[1]
		}
		if words := wordsRule.FindStringSubmatch(l); len(words) > 0 {
			for id, match := range wordsRule.FindAllStringSubmatch(l, -1) {
				if id > 0 {
					lRest += match[1]
				}
			}
			for id, match := range digitsRule.FindAllStringSubmatch(l, -1) {
				if id > 0 {
					lRest += match[1]
				}
			}
		}
		if digits := digitsRule.FindStringSubmatch(r); len(digits) > 0 {
			rNorm = digits[1]
		}
		if words := wordsRule.FindStringSubmatch(r); len(words) > 0 {
			for id, match := range wordsRule.FindAllStringSubmatch(r, -1) {
				if id > 0 {
					rRest += match[1]
				}
			}
			for id, match := range digitsRule.FindAllStringSubmatch(r, -1) {
				if id > 0 {
					rRest += match[1]
				}
			}
		}
		lNorm = Normalize(lNorm, width, parts)
		rNorm = Normalize(rNorm, width, parts)
		res := cmp(lNorm, rNorm)
		if res == 0 && (len(lRest) > 0 || len(rRest) > 0) {
			res = cmp(lRest, rRest)
		}
		return res
	}
}
