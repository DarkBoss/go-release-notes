package options

import (
	"github.com/karrick/golf"
	"reflect"
	"time"
)

type OptionActivator func(o *Option) error
type OptionSetter func(o *Option, v interface{}) error

type OptionHandler interface {
	Register(o *Option) error
	Activate(o *Option, v interface{}) error
}

type OptionHandlerBase struct {
	activator OptionSetter
}

func NewOptionHandlerBase(a OptionSetter) *OptionHandlerBase {
	return &OptionHandlerBase{a}
}

type BoolOpt struct { *OptionHandlerBase }

type IntOpt struct { *OptionHandlerBase }
type Int64Opt struct { *OptionHandlerBase }
type UintOpt struct { *OptionHandlerBase }
type Uint64Opt struct { *OptionHandlerBase }
type StringOpt struct { *OptionHandlerBase }
type FloatOpt struct { *OptionHandlerBase }
type Float64Opt struct { *OptionHandlerBase }
type DurationOpt struct { *OptionHandlerBase }

func NewBoolOpt(activate OptionSetter) *BoolOpt {
	return &BoolOpt{NewOptionHandlerBase(activate)}
}

func (this *BoolOpt) Register(o *Option) error {
	o.Kind = reflect.Bool
	if o.Short != 0 {
		o.Value = golf.BoolP(o.Short, o.Long, o.DefaultValue.(bool), o.Usage)
	} else {
		o.Value = golf.Bool(o.Long, o.DefaultValue.(bool), o.Usage)
	}
	return nil
}

func (this *BoolOpt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}

func NewIntOpt(activate OptionSetter) *IntOpt {
	return &IntOpt{
		NewOptionHandlerBase(activate),
	}
}

func (this *IntOpt) Register(o *Option) error {
	o.Kind = reflect.Int
	if o.Short != 0 {
		o.Value = golf.IntP(o.Short, o.Long, o.DefaultValue.(int), o.Usage)
	} else {
		o.Value = golf.Int(o.Long, o.DefaultValue.(int), o.Usage)
	}
	return nil
}

func (this *IntOpt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}

func NewInt64Opt(activate OptionSetter) *Int64Opt {
	return &Int64Opt{
		NewOptionHandlerBase(activate),
	}
}

func (this *Int64Opt) Register(o *Option) error {
	o.Kind = reflect.Int64
	if o.Short != 0 {
		o.Value = golf.Int64P(o.Short, o.Long, o.DefaultValue.(int64), o.Usage)
	} else {
		o.Value = golf.Int64(o.Long, o.DefaultValue.(int64), o.Usage)
	}
	return nil
}
func (this *Int64Opt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}

func NewUintOpt(activate OptionSetter) *UintOpt {
	return &UintOpt{
		NewOptionHandlerBase(activate),
	}
}


func (this *UintOpt) Register(o *Option, v interface{}) error {
	o.Kind = reflect.Uint
	if o.Short != 0 {
		o.Value = golf.UintP(o.Short, o.Long, o.DefaultValue.(uint), o.Usage)
	} else {
		o.Value = golf.Uint(o.Long, o.DefaultValue.(uint), o.Usage)
	}
	return nil
}

func (this *UintOpt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}

func NewUint64Opt(activate OptionSetter) *Uint64Opt {
	return &Uint64Opt{
		NewOptionHandlerBase(activate),
	}
}

func (this *Uint64Opt) Register(o *Option) error {
	o.Kind = reflect.Uint64
	if o.Short != 0 {
		o.Value = golf.Uint64P(o.Short, o.Long, o.DefaultValue.(uint64), o.Usage)
	} else {
		o.Value = golf.Uint64(o.Long, o.DefaultValue.(uint64), o.Usage)
	}
	return nil
}

func (this *Uint64Opt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}

func NewStringOpt(activate OptionSetter) *StringOpt {
	return &StringOpt{
		NewOptionHandlerBase(activate),
	}
}

func (this *StringOpt) Register(o *Option) error {
	o.Kind = reflect.String
	if o.Short != 0 {
		o.Value = golf.StringP(o.Short, o.Long, o.DefaultValue.(string), o.Usage)
	} else {
		o.Value = golf.String(o.Long, o.DefaultValue.(string), o.Usage)
	}
	return nil
}
func (this *StringOpt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}

func NewDurationOpt(activate OptionSetter) *DurationOpt {
	return &DurationOpt{
		NewOptionHandlerBase(activate),
	}
}

func (this *DurationOpt) Register(o *Option) error {
	o.Kind = reflect.Struct
	if o.Short != 0 {
		o.Value = golf.DurationP(o.Short, o.Long, o.DefaultValue.(time.Duration), o.Usage)
	} else {
		o.Value = golf.Duration(o.Long, o.DefaultValue.(time.Duration), o.Usage)
	}
	return nil
}

func (this *DurationOpt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}

func NewFloatOpt(activate OptionSetter) *FloatOpt {
	return &FloatOpt{
		NewOptionHandlerBase(activate),
	}
}

func (this *FloatOpt) Register(o *Option) error {
	o.Kind = reflect.Float32
	if o.Short != 0 {
		o.Value = golf.FloatP(o.Short, o.Long, o.DefaultValue.(float64), o.Usage)
	} else {
		o.Value = golf.Float(o.Long, o.DefaultValue.(float64), o.Usage)
	}
	return nil
}

func (this *FloatOpt) Activate(o *Option, v interface{}) error {
	return this.activator(o, v)
}