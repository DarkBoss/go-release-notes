package options

import (
	"bitbucket.org/DarkBoss/go-release-notes/log"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"github.com/imdario/mergo"
)

var appOptions *AppOptions

type AppOptions struct {
	RepoPath              string           `json:"-"`
	OutputFilenameFormat  string           `json:"outputFilenameFormat"`
	OutputFormat          string           `json:"outputFormat"`
	VerboseLevel          log.VerboseLevel `json:"verboseLevel"`
	ProjectName           string           `json:"projectName"`
	ProjectVersion        string           `json:"projectVersion"`
	DisplayLongAuthors    bool             `json:"displayLongAuthors"`
	UseProjectConfig      bool             `json:"useProjectConfig"`
	UseProjectTemplate    bool             `json:"useProjectTemplate"`
	ProjectFolderName     string           `json:"projectFolderName"`
	ProjectFileName       string           `json:"projectFileName"`
	ProjectTemplateName   string           `json:"projectTemplateName"`
	GenerateProjectConfig bool             `json:"generateProjectConfig"`
	NoteAuthor            string           `json:"noteAuthor"`
	TeamName              string           `json:"teamName"`
	TeamWords             string           `json:"teamWords"`

	Interactive bool `json:"interactive"`
	ShowHelp    bool `json:"-"`
	DumpConfig  bool `json:"-"`
	DumpNote    bool `json:"-"`
	ShowVersion bool `json:"-"`
}

func NewAppOptions(RepoPath string, outputFilenameFormat string, verbose log.VerboseLevel) *AppOptions {
	return &AppOptions{
		RepoPath,
		outputFilenameFormat,
		DefaultOptionOutputFormat,
		verbose,
		DefaultOptionProjectName,
		DefaultOptionProjectVersion,
		DefaultOptionLongAuthor,
		DefaultOptionUseProjectConfig,
		DefaultOptionUseProjectTemplate,
		DefaultOptionProjectFolderName,
		DefaultOptionProjectFileName,
		DefaultOptionProjectTemplateName,
		DefaultOptionGenerateConfig,
		DefaultOptionNoteAuthor,
		DefaultOptionProjectTeamName,
		DefaultOptionProjectTeamWords,

		true,
		false,
		false,
		false,
		false,
	}
}

func NewAppOptionsWithDefaults() *AppOptions {
	return NewAppOptions(DefaultOptionRepoPath, DefaultOptionOutputFilenameFormat, DefaultOptionVerbose)
}

func (this *AppOptions) Merge(with ...*AppOptions) error {
	for _, w := range with {
		if err := mergo.Merge(this, w, mergo.WithOverride); err != nil {
			return err
		}
	}
	return nil
}

func (this *AppOptions) Load(path string) error {
	var data []byte
	var err error
	if data, err = ioutil.ReadFile(path); err != nil {
		return err
	}
	if err = json.Unmarshal(data, this); err != nil {
		return err
	}
	log.Printf(log.VerboseMedium, "[+] Loaded configuration @ %s\n", path)
	return nil
}

func (this *AppOptions) Save(path string) error {
	if data, err := json.MarshalIndent(this, "", "\t"); err != nil {
		return err
	} else if err = ioutil.WriteFile(path, data, 0777); err != nil {
		return err
	}
	fmt.Printf("[+] Saved configuration @ %s\n", path)
	return nil
}

func Update(opts ...AppOptions) error {
	*appOptions = AppOptions{}
	for _, o := range opts {
		//SetApp(usrOptions)
		if err := appOptions.Merge(&o); err != nil {
			return err
		}
	}
	// cli options already triggered
	//if err = options.GetApp().Merge(cliOptions); err != nil {
	//	return nil, err
	//}
	return nil
}

func GetApp() *AppOptions {
	return appOptions
}

func SetApp(opts *AppOptions) {
	*appOptions = *opts
}

func GetUserHomeDir() string {
	var user, userErr = user.Current()
	if userErr != nil {
		panic(userErr)
	}
	return user.HomeDir
}

func GetDefaultUserConfigLocations(appName string) [][]string {
	return [][]string{
		{"$HOME", appName + ".json"},
	}
}

func ExpandPath(parts ...string) string {
	data := map[string]string{
		"home": GetUserHomeDir(),
		"tmp":  os.TempDir(),
	}
	path := strings.Replace(filepath.Join(parts...), "$HOME", data["home"], -1)
	path = strings.Replace(path, "$TMPDIR", data["tmp"], -1)
	path = strings.Replace(path, "$TMP", data["tmp"], -1)
	return path
}

func FindExistingFile(locations ...[]string) (path string, found bool, err error) {
	for _, f := range locations {
		path = ExpandPath(f...)
		_, err = os.Stat(path)
		if err == nil || os.IsExist(err) {
			return path, true, nil
		} else {
			fmt.Fprintf(os.Stderr, "[-] location doesn't match '%s'\n", path)
		}
	}
	return path, false, fmt.Errorf("no locations match '%s', in %v", path, locations)
}

func init() {
	appOptions = &AppOptions{}
}
