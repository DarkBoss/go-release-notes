package options

import "bitbucket.org/DarkBoss/go-release-notes/log"

const (
	DefaultOptionRepoPath             = "."
	DefaultOptionVerbose              = log.VerboseNone
	DefaultOptionOutputFilenameFormat = "note-{{.Project.Name}}-{{.Project.Version}}.{{.Format.Ext}}"
	DefaultOptionOutputFormat         = "html"
	DefaultOptionProjectName          = "my_project"
	DefaultOptionProjectVersion       = "1.0.0.0"
	DefaultOptionLongAuthor           = true
	DefaultOptionUseProjectConfig     = true
	DefaultOptionUseProjectTemplate   = true
	DefaultOptionProjectFolderName    = ".release-note"
	DefaultOptionProjectFileName      = "release-note.json"
	DefaultOptionProjectTemplateName  = "release-note.tmpl"
	DefaultOptionGenerateConfig       = false
	DefaultOptionNoteAuthor           = ""
	DefaultOptionProjectTeamName      = "Dream Team"
	DefaultOptionProjectTeamWords     = ""
)
