package options

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"reflect"
	"testing"
)

func TestAppOptions_Load(t *testing.T) {
	t.Run("loaded config should match defaults", func(t *testing.T) {
		path := filepath.Join(os.TempDir(), "test-go-release-notes")
		dfltOpts := NewAppOptionsWithDefaults()
		dfltOpts.RepoPath = ""
		dfltOpts.ShowHelp = false
		if data, err := json.MarshalIndent(dfltOpts, "", " "); err != nil {
			t.Fatalf("Load() failed to serialize app options: %s", err.Error())
		} else {
			if err := ioutil.WriteFile(path, data, 0777); err != nil {
				t.Fatalf("Load() failed to write temp file: %s", err.Error())
			}
			opts := &AppOptions{}
			if err := opts.Load(path); err != nil {
				t.Fatalf("Load() %s", err.Error())
			}
			if !reflect.DeepEqual(opts, dfltOpts) {
				t.Fatalf("Load() = %v, want %v", *opts, *dfltOpts)
			} else {
				t.Logf("Load() = %s", data)
			}
		}
	})
}

func TestAppOptions_Save(t *testing.T) {
	t.Run("saved config should match defaults", func(t *testing.T) {
		path := filepath.Join(os.TempDir(), "test-go-release-notes")
		dfltOpts := NewAppOptionsWithDefaults()
		opts := AppOptions{}
		if err := dfltOpts.Save(path); err != nil {
			t.Errorf("Save() failed to save options: %s", err.Error())
		} else if data, err := ioutil.ReadFile(path); err != nil {
			t.Errorf("Save() failed read file '%s': %s", path, err.Error())
		} else if err = json.Unmarshal(data, &opts); err != nil {
			t.Errorf("Save() %s", err.Error())
		} else {
			dfltOpts.RepoPath = ""
			dfltOpts.ShowHelp = false
			if !reflect.DeepEqual(opts, *dfltOpts) {
				t.Errorf("Save() = %v, want %v", opts, dfltOpts)
			} else {
				t.Logf("Save() = %s", string(data))
			}
		}
	})
}

func TestNewAppOptionsWithDefaults(t *testing.T) {
	tests := []struct {
		name string
		want *AppOptions
	}{
		{"NewAppOptionWithDefaults() instance should reflect defaults", &AppOptions{
			DefaultOptionRepoPath,
			DefaultOptionOutputFilenameFormat,
			DefaultOptionOutputFormat,
			DefaultOptionVerbose,
			DefaultOptionProjectName,
			DefaultOptionProjectVersion,
			DefaultOptionLongAuthor,
			DefaultOptionUseProjectConfig,
			DefaultOptionUseProjectTemplate,
			DefaultOptionProjectFolderName,
			DefaultOptionProjectFileName,
			DefaultOptionProjectTemplateName,
			DefaultOptionGenerateConfig,
			DefaultOptionNoteAuthor,
			DefaultOptionProjectTeamName,
			false,
			false,
			false,
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAppOptionsWithDefaults(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAppOptionsWithDefaults() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExpandPath(t *testing.T) {
	type args struct {
		parts []string
	}
	home := ""
	tmp := os.TempDir()
	if usr, err := user.Current(); err != nil {
		t.Fatal(err)
	} else {
		home = usr.HomeDir
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"should expand user home dir", args{[]string{"$HOME", "test"}}, filepath.Join(home, "test")},
		{"should expand temp dir", args{[]string{"$TMP", "test"}}, filepath.Join(tmp, "test")},
		{"should expand temp dir 2", args{[]string{"$TMPDIR", "test"}}, filepath.Join(tmp, "test")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ExpandPath(tt.args.parts...); got != tt.want {
				t.Errorf("ExpandPath() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCLICanOverrideLoadedConfig(t *testing.T) {
	usrOpts := NewAppOptionsWithDefaults()
	cliOpts := &AppOptions{ProjectName: "test-project"}
	if err := usrOpts.Merge(cliOpts); err != nil {
		t.Fatal(err)
	}
	if cliOpts.ProjectName != "test-project" {
		t.Fatal("CLICanOverrideLoadedConfig() = false")
	}
}

func TestFindExistingFile(t *testing.T) {
	type args struct {
		locations [][]string
	}
	tmpDir := os.TempDir()
	tmpFile := []string{"$TMP", "test-go-release-notes"}
	realTmpFile := filepath.Join(tmpDir, "test-go-release-notes")
	tests := []struct {
		name      string
		args      args
		wantPath  string
		wantFound bool
		wantErr   bool
	}{
		{"should find file tmp file", args{[][]string{
			tmpFile,
		}}, realTmpFile, true, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ioutil.WriteFile(tt.wantPath, []byte{}, 0777); err != nil {
				t.Errorf("FindExistingFile() %s", err.Error())
			} else {
				defer func() {
					os.Remove(tt.wantPath)
				}()
				gotPath, gotFound, err := FindExistingFile(tt.args.locations...)
				if (err != nil) != tt.wantErr {
					t.Errorf("FindExistingFile() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
				if gotPath != tt.wantPath {
					t.Errorf("FindExistingFile() gotPath = %v, want %v", gotPath, tt.wantPath)
				}
				if gotFound != tt.wantFound {
					t.Errorf("FindExistingFile() gotFound = %v, want %v", gotFound, tt.wantFound)
				}
			}
		})
	}
}

func TestGetDefaultUserConfigLocations(t *testing.T) {
	t.Run("default user config path should not be empty", func(t *testing.T) {
		if got := GetDefaultUserConfigLocations("release-notes"); len(got) == 0 {
			t.Errorf("GetDefaultUserConfigLocations() = %v, shouldn't be empty!", got)
		}
	})
}
