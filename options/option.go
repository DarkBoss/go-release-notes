package options

import (
	"fmt"
	"reflect"
	"time"
)

var optionList = &OptionList{}

func RegisterOption(name string, short rune, long string, defaultVal interface{}, usage string, handler OptionHandler) (*Option, error) {
	o := NewOption(name, short, long, defaultVal, usage, handler)
	if err := optionList.Register(o); err != nil {
		return nil, err
	}
	return o, nil
}

type Option struct {
	Name         string
	Short        rune
	Long         string
	DefaultValue interface{}
	Value        interface{}
	Usage        string
	Kind         reflect.Kind
	Handler 	OptionHandler
}

func NewOption(name string, short rune, long string, defaultVal interface{}, usage string, handler OptionHandler) *Option {
	return &Option{
		name,
		short,
		long,
		defaultVal,
		nil,
		usage,
		0,
		handler,
	}
}

func (this *Option) GetBool() bool {
	return *this.Value.(*bool)
}

func (this *Option) GetDuration() time.Duration {
	return *this.Value.(*time.Duration)
}

func (this *Option) GetFloat() float32 {
	return *this.Value.(*float32)
}

func (this *Option) GetFloat64() float64 {
	return *this.Value.(*float64)
}

func (this *Option) GetInt() int {
	return *this.Value.(*int)
}

func (this *Option) GetInt64() int64 {
	return *this.Value.(*int64)
}

func (this *Option) GetUint() uint {
	return *this.Value.(*uint)
}

func (this *Option) GetUint64() uint64 {
	return *this.Value.(*uint64)
}

func (this *Option) GetString() string {
	return *this.Value.(*string)
}

func (this *Option) String() string {
	v := this.GetValue()
	ret := ""
	switch this.Kind {
	case reflect.Bool:
		ret = fmt.Sprintf("%t", v)
	case reflect.Int:
		ret = fmt.Sprintf("%d", v)
	case reflect.Int64:
		ret = fmt.Sprintf("%d", v)
	case reflect.Uint:
		ret = fmt.Sprintf("%d", v)
	case reflect.Uint64:
		ret = fmt.Sprintf("%d", v)
	case reflect.Float64:
		ret = fmt.Sprintf("%4.3f", v)
	case reflect.Struct:
		fallthrough
	default:
		ret = fmt.Sprintf("%+v", v)
	}
	return ret
}

func (this *Option) GetValue() interface{} {
	var ret interface{}
	switch this.Kind {
	case reflect.String:
		ret = *this.Value.(*string)
	case reflect.Bool:
		ret = *this.Value.(*bool)
	case reflect.Int:
		ret = *this.Value.(*int)
	case reflect.Int64:
		ret = *this.Value.(*int64)
	case reflect.Uint:
		ret = *this.Value.(*uint)
	case reflect.Uint64:
		ret = *this.Value.(*uint64)
	case reflect.Float64:
		ret = *this.Value.(*float64)
	case reflect.Struct:
		fallthrough
	default:
		ret = this.Value
	}
	return ret
}


type OptionList []*Option

func (this *OptionList) Register(o *Option) error {
	if err := o.Handler.Register(o); err != nil {
		return err
	}
	*this = append(*this, o)
	return nil
}

type OptionFilterPredicate = func(o *Option) bool

func (this *OptionList) Filter(predicate OptionFilterPredicate) *OptionList {
	ret := &OptionList{}
	for _, o := range *this {
		if predicate(o) {
			*ret = append(*ret, o)
		}
	}
	return ret
}

func (this *OptionList) Find(predicate OptionFilterPredicate) *Option {
	for _, o := range *this {
		if predicate(o) {
			return o
		}
	}
	return nil
}

func NameEquals(value string) OptionFilterPredicate {
	return func(o *Option) bool {
		return o.Name == value
	}
}

func ShortFlagEquals(value string) OptionFilterPredicate {
	return func(o *Option) bool {
		r := rune(0)
		if len(value) == 1 {
			r = rune(value[0])
			return o.Short == r
		}
		return false
	}
}

func LongFlagEquals(value string) OptionFilterPredicate {
	return func(o *Option) bool {
		return o.Long == value
	}
}

func (this *OptionList) FindByName(name string) *Option {
	return this.Find(NameEquals(name))
}

func (this *OptionList) FindByShortFlag(name string) *Option {
	return this.Find(ShortFlagEquals(name))
}

func (this *OptionList) FindByLongFlag(name string) *Option {
	return this.Find(LongFlagEquals(name))
}

func GetAll() *OptionList {
	return optionList
}