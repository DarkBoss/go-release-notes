package main

import (
	"bitbucket.org/DarkBoss/go-release-notes/app"
)

var AppData = &app.AppData{
	Name:   "go-release-notes",
	Era:    "2019-2021",
	Desc:   "A small go tool able to generate a formatted release note from a git\nrepository. It's fully customisable, so enjoy!",
	Author: "Morgan 'DarkBoss' Welsch",
}

func main() {
	defer app.Shutdown()
	if appInst, err := app.Create(AppData, true); appInst == nil || err != nil {
		panic(err)
	} else if err = appInst.Run(); err != nil {
		panic(err)
	}
}
