package log

type VerboseLevel uint8

const (
	VerboseNone   VerboseLevel = iota
	VerboseLow                 = iota
	VerboseMedium              = iota
	VerboseHigh                = iota
)

var verbose = VerboseNone

func (this VerboseLevel) String() string {
	return AllVerboseLevels()[uint8(this)]
}

func AllVerboseLevels() map[uint8]string {
	return map[uint8]string{
		uint8(VerboseNone):   "none",
		uint8(VerboseLow):    "low",
		uint8(VerboseMedium): "medium",
		uint8(VerboseHigh):   "high",
	}
}

func SetVerbose(lvl VerboseLevel) {
	verbose = lvl
}

func Verbose() VerboseLevel {
	return verbose
}

func IfVerbose(lvl VerboseLevel, fn func() error) error {
	if verbose >= lvl {
		return fn()
	}
	return nil
}