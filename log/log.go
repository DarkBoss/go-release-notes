package log

import (
	"fmt"
)

var stdLog = New("default.log")

func SetDefault(l *Logger) {
	stdLog = l
}

func Default() *Logger {
	return stdLog
}

func Fatal(args ...interface{}) {
	stdLog.Fatal(args...)
}

func Fatalln(args ...interface{}) {
	stdLog.Fatalln(args...)
}

func Fatalf(s string, args ...interface{}) {
	stdLog.Fatalf(s, args...)
}

func Panic(args ...interface{}) {
	stdLog.Panic(args...)
}

func Panicln(args ...interface{}) {
	stdLog.Panicln(args...)
}

func Panicf(s string, args ...interface{}) {
	stdLog.Panicf(s, args...)
}

func Errorf(s string, args ...interface{}) error {
	err := fmt.Errorf(s, args...)
	stdLog.Printf(VerboseNone, "error: %s", err.Error())
	return err
}

func Error(err error) error {
	stdLog.Printf(VerboseNone, "error: %s", err.Error())
	return err
}

func Print(min VerboseLevel, args ...interface{}) {
	stdLog.Print(min, args...)
}

func Println(min VerboseLevel, args ...interface{}) {
	stdLog.Println(min, args...)
}

func Printf(min VerboseLevel, s string, args ...interface{}) {
	stdLog.Printf(min, s, args...)
}
