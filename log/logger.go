package log

import (
	"fmt"
	"log"
	"os"
	"time"
)

type Logger struct {
	*log.Logger
}

func New(name string, flags ...int) *Logger {
	out := os.Stderr
	rflags := 0
	for _, f := range flags {
		rflags |= f
	}
	l := &Logger{
		log.New(out, fmt.Sprintf("[%s] ", name), rflags),
	}
	return l
}

func (this *Logger) Print(min VerboseLevel, args ...interface{}) error {
	return IfVerbose(min, func() error {
		this.Logger.Print(fmt.Sprintf("[%s] %s", Timestamp(), fmt.Sprint(args...)))
		return nil
	})
}
func (this *Logger) Println(min VerboseLevel, args ...interface{}) error {
	return IfVerbose(min, func() error {
		this.Logger.Println(fmt.Sprintf("[%s] %s", Timestamp(), fmt.Sprint(args...)))
		return nil
	})
}

func Timestamp() string {
	t := time.Now()
	y, m, d := t.Date()
	h, i, sec := t.Hour(), t.Minute(), t.Second()
	return fmt.Sprintf("%d-%d-%d %d:%d:%d", y, m, d, h, i, sec)
}

func (this *Logger) Printf(min VerboseLevel, s string, args ...interface{}) error {
	return IfVerbose(min, func() error {
		this.Logger.Printf("[%s] %s", Timestamp(), fmt.Sprintf(s, args...))
		return nil
	})
}

func (this *Logger) Fatal(args ...interface{}) {
	this.Logger.Fatal(fmt.Sprintf("[%s] %s", Timestamp(), fmt.Sprint(args...)))
}
func (this *Logger) Fatalln(args ...interface{}) {
	this.Logger.Fatalln(fmt.Sprintf("[%s] %s", Timestamp(), fmt.Sprint(args...)))
}

func (this *Logger) Fatalf(s string, args ...interface{}) {
	this.Logger.Fatalf("[%s] %s", Timestamp(), fmt.Sprintf(s, args...))
}

func (this *Logger) Panic(args ...interface{}) {
	this.Logger.Panic(fmt.Sprintf("[%s] %s", Timestamp(), fmt.Sprint(args...)))
}
func (this *Logger) Panicln(args ...interface{}) {
	this.Logger.Panicln(fmt.Sprintf("[%s] %s", Timestamp(), fmt.Sprint(args...)))
}

func (this *Logger) Panicf(s string, args ...interface{}) {
	this.Logger.Panicf("[%s] %s", Timestamp(), fmt.Sprintf(s, args...))
}

