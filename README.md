# go-release-notes

The small release note maker that could...

- Allows easy release note generation
- Support multiple formats
- Support project & global level configuration

### Dependencies

#### Automatic installation    

```bash
cd go-release-notes
go get ./...
go install
```

#### Manual installation

- Run the following go-get commands:

```bash
go get github.com/karrick/golf
go get gopkg.in/src-d/go-git.v4
go get github.com/tsuyoshiwada/go-gitlog
go get github.com/cweill/gotests
go get github.com/imdario/mergo
go get github.com/tcnksm/go-input
go get -u gopkg.in/src-d/go-billy.v4/...
go get -u github.com/gobuffalo/packr/v2/packr2
go get -u github.com/gobuffalo/packr/v2
```

OR:
- run ```make install_deps```

### Build

This project now uses packer to embed it's resources, just replace ```go build``` with ```packr2 build```
- using make:
    ```make && PREFIX=/usr/local make install```

- using native go build system:
    ```bash
    packr2 build -o go-release-notes.exe
    chmod +x go-release-notes.exe
    ```

#### The following linker flags are used

 * -ldflags="-X bitbucket.org/DarkBoss/go-release-notes/app.gVersion=${VERSION}"
   
   Where version is the build version number, formatted as `Major.Minor.Revision.Build`. 
 
### Known bugs / Pending features

- If you give a format and a template is given, the template will override the selected format's internal template
  thus producing an output in a different format that expected with the selected format's extension.
  
  e.g: --format html with a template formatted as json will produce a file.html whose content would be json.

### Usage

The command works this way:
- You can get the full usage of this program by running ```go-release-notes -h/--help```.
- It must be run in the folder the project resides, or if not, git the -C option

#### Generate a project configuration

The user would be prompted for values and two files would be created: 
- a project configuration file
- a template file

```bash
./go-release-notes.exe --generate-config
```

#### Give project details via command-line

Instead of using project file you may want to give command-line parameters. Those settings override the 
ones loaded from global config but the project files override everything.
Pick a flow and stand by it! 

```bash
./go-release-notes.exe --project-name "name" --team "teamName" --author "releaseNoteGuy" --version "1.0.0.0" 
```

### Switching formats

Few output formats are supported:
- json
- csv
- html

```bash
./go-release-notes --format=csv
```

### Template System

This project uses the go native templating system (namely text/template).
Refer to golang's official documentation to know how to proceed.
The available template variables are listed by the `--dump-note` CLI flag.

### Dumping internal state before writing

For debuggin purposes you may want to dump the internal state before generating a note,
the following let's you accomplish this:
```bash
./go-release-notes.exe --dump-config    # dumps the combined cli + loaded config
./go-release-notes.exe --dump-note      # dumps the full release note 
```


### Author

This project was made especially for the 2AV dev-team with love by Morgan Welsch.

### Contribute

All contributors are welcome, pull request time!