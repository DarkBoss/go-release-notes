#!/usr/bin/env bash

####################################################################
# This script unpacks the compressed example repositories          #
#                                                                  #
# Author: Morgan 'DarkBoss' Welsch @ 2019                          #
####################################################################

LOG_FILE=.unpacked_repos.log
LOOKUP="*${EXTENSION}"
EXTENSION=.zip
UNPACK_DIR=build/repos

function unpack_repo {
    REPO=$1
    echo Unpacking $(basename $REPO) to ${UNPACK_DIR}...
    #tar -C ${UNPACK_DIR} -xvf $REPO > $LOG_FILE
    pushd build/repos
    unzip ${REPO}
    popd
}

mkdir -p ${UNPACK_DIR}
for REPO in $(find repos -name "$LOOKUP"); do
    
done