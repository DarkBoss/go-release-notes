package main

import (
	"bitbucket.org/DarkBoss/go-release-notes/scripts/package/packager"
	"fmt"
	"log"
	"runtime"
)

const (
	appName        = "go-release-notes"
	appVersionFile = ".version"
)

var (
	pkg *packager.Packager
	appBinary string
	appFiles  map[string][]string
)

func init() {
	appBinary = appName
	if runtime.GOOS == "windows" {
		appBinary += ".exe"
	}
	var err error
	if pkg, err = packager.New(appName, appVersionFile, "zip"); err != nil {
		log.Fatalf("error: %s", err.Error())
	}
	appFiles = map[string][]string{
		"doc": {
			"README.md",
			"CHANGELOG.md",
		},
		"bin": {
			appBinary,
		},
		"/": {
			".version",
		},
	}
}

func main() {
	var err error
	fmt.Printf("Packaging %s...\n", appName)
	for k, files := range appFiles {
		d := pkg.Dir(k)
		for _, f := range files {
			d.File(f)
		}
	}
	if err = pkg.Write(); err != nil {
		log.Fatalf("error: %s", err.Error())
	}
}
