package version

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

var (
	ErrCannotReadVersionFile = errors.New("%s: cannot read version file: %s")
	ErrCorruptVersionFile    = errors.New("corrupt version file")
	ErrVersionPartInvalid    = errors.New("part #%d is invalid, %s")
)

type VersionPart = int16

type Version struct {
	Major    VersionPart `json:"major"`
	Minor    VersionPart `json:"minor"`
	Revision VersionPart `json:"revision"`
	Build    VersionPart `json:"build"`
	Tags     string      `json:"tags"`
}

func (this *Version) ParseFile(path string) (err error) {
	var data []byte
	if data, err = ioutil.ReadFile(path); err != nil {
		return fmt.Errorf(ErrCannotReadVersionFile.Error(), path, err.Error())
	}
	err = this.Parse(data)
	return err
}

func (this *Version) ParseString(s string) error {
	return this.Parse([]byte(s))
}

func (this *Version) Parse(data []byte) error {
	type part_extract struct {
		value  int64
		svalue string
		err    error
	}
	s := strings.TrimSpace(string(data))
	parts := strings.Split(s, ".")
	if len(parts) != 4 {
		return fmt.Errorf(ErrCorruptVersionFile.Error())
	}
	extracts := [4]part_extract{{svalue: parts[0]}, {svalue: parts[1]}, {svalue: parts[2]}, {svalue: parts[3]}}
	for i, ex := range extracts {
		if extracts[i].value, ex.err = strconv.ParseInt(ex.svalue, 10, 32); ex.err != nil {
			return fmt.Errorf(ErrVersionPartInvalid.Error(), i+1, ex.err)
		}
	}
	this.Major = VersionPart(extracts[0].value)
	this.Minor = VersionPart(extracts[1].value)
	this.Revision = VersionPart(extracts[2].value)
	this.Build = VersionPart(extracts[3].value)
	return nil
}

func (this *Version) String() string {
	tags := func() string {
		if len(this.Tags) > 0 {
			return fmt.Sprintf("-%s", this.Tags)
		}
		return ""
	}()
	return fmt.Sprintf("%d.%d.%d.%d%s", this.Major, this.Minor, this.Revision, this.Build, tags)
}

func Load(path string) (*Version, error) {
	var ret *Version = &Version{}
	if err := ret.ParseFile(path); err != nil {
		return nil, err
	}
	return ret, nil
}
