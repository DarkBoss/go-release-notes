package packager

import (
	"archive/zip"
	"bitbucket.org/DarkBoss/go-release-notes/scripts/package/version"
	"errors"
	"fmt"
	"github.com/dustin/go-humanize"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var (
	versionSanitizer = regexp.MustCompile(`[^0-9a-z]`)
)

type Packager struct {
	appName    string
	appVersion *version.Version
	targetPath string
	sourcePath string
	targetExt  string
	rootFolder *PackageDir
}

func New(appName, appVersionFile, ext string) (*Packager, error) {
	var err error
	var cwd string
	var v *version.Version
	if cwd, err = os.Getwd(); err != nil {
		return nil, err
	}
	if v, err = version.Load(appVersionFile); err != nil {
		log.Fatalf("%s: failed to load version", err.Error())
	}
	ret := &Packager{
		appName:    appName,
		appVersion: v,
		targetPath: "",
		sourcePath: cwd,
		targetExt:  ext,
		rootFolder: &PackageDir{"/", PackageEntryMap{}},
	}
	ret.rootFolder.Path = fmt.Sprintf("%s-%s", ret.AppName(), ret.AppVersion().String())
	ret.targetPath = fmt.Sprintf("build/%s.%s", ret.rootFolder.Path, ret.TargetExt())
	return ret, nil
}

func (this *Packager) AppName() string {
	return this.appName
}

func (this *Packager) AppVersion() *version.Version {
	return this.appVersion
}

func (this *Packager) TargetPath() string {
	return this.targetPath
}

func (this *Packager) TargetExt() string {
	return this.targetExt
}

func (this *Packager) SourcePath() string {
	return this.sourcePath
}

func (this *Packager) SaneAppVersion() string {
	return versionSanitizer.ReplaceAllString(this.appVersion.String(), "")
}

type PackageFile struct {
	Path string
}

type PackageEntryMap = map[string]interface{}

type PackageDir struct {
	Path    string
	Entries PackageEntryMap
}

func (this *PackageDir) File(path string) *PackageFile {
	this.Entries[path] = &PackageFile{path}
	return this.Entries[path].(*PackageFile)
}

func (this *PackageDir) Dir(path string, files ...string) *PackageDir {
	d := &PackageDir{path, PackageEntryMap{}}
	for _, f := range files {
		d.File(f)
	}
	this.Entries[path] = d
	return this.Entries[path].(*PackageDir)
}

func (this *Packager) File(k string) *PackageFile {
	return this.rootFolder.File(k)
}

func (this *Packager) Dir(k string) *PackageDir {
	return this.rootFolder.Dir(k)
}

func (this *Packager) RootDir() *PackageDir {
	return this.rootFolder
}

func (this *Packager) WriteFileEntry(zw *zip.Writer, f *PackageFile, mountPoint string, level int) error {
	indent := strings.Repeat("\t", level)
	closeWriter := func(closer func() error) {
		if err := closer(); err != nil {
			panic(err.Error())
		}
	}
	if in, err := os.Open(f.Path); err != nil {
		return err
	} else {
		defer closeWriter(in.Close)
		// Get the file information
		info, err := in.Stat()
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		// Using FileInfoHeader() above only uses the basename of the file. If we want
		// to preserve the folder structure we can overwrite this with the full path.
		header.Name = filepath.Join(mountPoint, f.Path)

		// Change to deflate to gain better compression
		// see http://golang.org/pkg/archive/zip/#pkg-constants
		//header.Method = zip.Deflate
		header.Method = zip.Store

		if writer, err := zw.CreateHeader(header); err != nil {
			return err
		} else {
			if _, err = io.Copy(writer, in); err != nil {
				return err
			}
		}
		if err = zw.Flush(); err != nil {
			return err
		}
		fmt.Printf("%s-> add '%s' - %s @ %s\n", indent, f.Path, humanize.Bytes(uint64(info.Size())), mountPoint)

	}
	return nil
}

func (this *Packager) WriteDirEntry(zw *zip.Writer, d *PackageDir, mountPoint string, level int) error {
	var err error
	indent := strings.Repeat("\t", level)
	fmt.Printf("%s+ create dir %s\n", indent, d.Path)
	for _, p := range d.Entries {
		switch p.(type) {
		case *PackageFile:
			path := filepath.Join(mountPoint, d.Path)
			if err = this.WriteFileEntry(zw, p.(*PackageFile), path, level+1); err != nil {
				return err
			}
		case *PackageDir:
			path := filepath.Join(mountPoint, d.Path)
			if err = this.WriteDirEntry(zw, p.(*PackageDir), path, level+1); err != nil {
				return err
			}
		default:
			return errors.New("unknown package entry type")
		}
	}
	return nil
}

func (this *Packager) WriteEntries(zw *zip.Writer) error {
	var err error
	for _, e := range this.RootDir().Entries {
		switch e.(type) {
		case *PackageFile:
			f := e.(*PackageFile)
			if err = this.WriteFileEntry(zw, f, this.RootDir().Path, 0); err != nil {
				return err
			}
		case *PackageDir:
			dir := e.(*PackageDir)
			if err = this.WriteDirEntry(zw, dir, this.RootDir().Path, 0); err != nil {
				return err
			}
		default:
			return errors.New("unknown package entry type")
		}
	}
	return nil
}
func (this *Packager) Write() error {
	zipfile, err := os.Create(this.targetPath)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	archive := zip.NewWriter(zipfile)
	defer archive.Close()

	if err = this.WriteEntries(archive); err != nil {
		return err
	}
	return nil
}

func (this *Packager) addZipFiles(archive *zip.Writer, source string) error {
	info, err := os.Stat(source)
	if err != nil {
		return nil
	}
	var baseDir string
	if info.IsDir() {
		baseDir = filepath.Base(source)
	}
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}

	absSource := ""
	if baseDir != "" {
		if absSource, err = filepath.Abs(source); err != nil {
			return err
		} else {
			header.Name = filepath.Join(baseDir, strings.TrimPrefix(absSource, source))
		}
	}

	if info.IsDir() {
		header.Name += "/"
	} else {
		header.Method = zip.Deflate
	}

	writer, err := archive.CreateHeader(header)
	if err != nil {
		return err
	}

	if !info.IsDir() {
		file, err := os.Open(absSource)
		if err != nil {
			return err
		}
		defer file.Close()
		if _, err = io.Copy(writer, file); err != nil {
			return err
		}
	}
	return nil
}
