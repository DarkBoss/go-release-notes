#!/usr/bin/env bash

PROJECT_CHANGELOG_FILE=CHANGELOG.md

OLD_VERSION=$(git tag -l --sort=taggerdate | head -n 1)
CUR_VERSION=

CUR_CHANGES_FILE=$(mktemp)
ALL_CHANGES_FILE=$(mktemp)

LOG_ARGS=-"-oneline --graph --first-parent"

declare -A OPTIONS
OPTIONS=(
    [view]=0
    [help]=0
)

function usage {
    echo -e "$0 <version> [OPTIONS...]\n"
    echo "* version"
    echo -e "\tMAJOR.MINOR.PATCH.BUILD\n"
    echo "* options:"
    echo -e "\t-h, --help:"
    echo -e "\t\tshow this help screen"
    echo -e "\t-V, --view:"
    echo -e "\t\tonly view the generated changelog, do not write it to project log"
    echo -e "\t-A, --all:"
    echo -e "\t\tget the whole changelog"
    exit 1
}

function check_usage {
    if [[ -z "${CUR_VERSION}" && ${OPTIONS[all]} == 0 ]]; then
        echo "Missing version!" > /dev/stderr
        usage;
    fi
}

function parse_args {
    while [ $# -gt 0 ]; do
        case $1 in
        -V|--view)
            OPTIONS[view]=1 ;;
        -h|--help)
            OPTIONS[help]=1 ;;
        -A|--all)
            OPTIONS[all]=1 ;;
        *)
            if [ -z "${CUR_VERSION}" ]; then
                CUR_VERSION=$1
            else
                echo "Unknown option $1"
                usage
            fi
        esac
        shift
    done
}

function find_tag {
    git tag -l | grep $1
}

function check_release_doesnt_exist {
    if [ ${OPTIONS[view]} = 0 ]; then
        if [ ! -z "$(cat ${PROJECT_CHANGELOG_FILE} | grep "### Release ${CUR_VERSION}")" ]; then
            echo "Release '${CUR_VERSION}' already exist in changelog!"
            exit 1
        fi
    fi
    if [ ${OPTIONS[view]} = 1 ]; then
        echo "Viewing release '${CUR_VERSION}' changelog..."
    else
        echo "Preparing release '${CUR_VERSION}' changelog..."
    fi
}

function grab_commits {
    if [[ ! -z "${OLD_VERSION}" && ${OPTIONS[all]} == 0 ]]; then
        if [ -z $(find_tag ${OLD_VERSION}) ]; then
            echo "${OLD_VERSION}: tag not found";
            exit 1;
        fi
        LOG_ARGS="HEAD --not ${OLD_VERSION} ${LOG_ARGS}"
    fi
}

function write_changelog {
    SINCE=""
    if [ ! -z "${OLD_VERSION}" ]; then
        SINCE="since ${OLD_VERSION}"
    fi

    check_release_doesnt_exist

    if [[ ${OPTIONS[view]} == 0 && ${OPTIONS[all]} = 1 ]]; then
        echo "All changes mode only available while viewing changelog, use --view!" > /dev/stderr
        exit 1
    fi

    touch ${PROJECT_CHANGELOG_FILE}
    echo "### Release ${CUR_VERSION}" > ${CUR_CHANGES_FILE}
    echo >> ${CUR_CHANGES_FILE}
    echo "#### Changeset $SINCE" >> ${CUR_CHANGES_FILE}
    echo >> ${CUR_CHANGES_FILE}
    git log ${LOG_ARGS} | xargs -I{} echo "  {}" >> ${CUR_CHANGES_FILE}
    if [ ${OPTIONS[view]} = 0 ]; then
        nano ${CUR_CHANGES_FILE}
    fi
    cp ${CUR_CHANGES_FILE} ${ALL_CHANGES_FILE}
    echo >> ${ALL_CHANGES_FILE}
    cat ${PROJECT_CHANGELOG_FILE} >> ${ALL_CHANGES_FILE}
    if [ ${OPTIONS[view]} = 0 ]; then
        cp ${ALL_CHANGES_FILE} ${PROJECT_CHANGELOG_FILE}
        echo "> $(cat ${CUR_CHANGES_FILE} | wc -l) lines added"
    else
        if [[ ${OPTIONS[all]} = 1 ]]; then
            less ${ALL_CHANGES_FILE}
            echo "> $(cat ${ALL_CHANGES_FILE} | wc -l) lines viewed"
        else
            less ${CUR_CHANGES_FILE}
            echo "> $(cat ${CUR_CHANGES_FILE} | wc -l) lines viewed"
        fi
    fi
}

parse_args $*
check_usage

if [ ${OPTIONS[help]} = 1 ]; then
    usage
fi
grab_commits

write_changelog