#!/usr/bin/env bash

####################################################################
# This script automatically installs go and system packages.       #
# The software is dependent on those packages.                     #
#                                                                  #
# Author: Morgan 'DarkBoss' Welsch @ 2019                          #
####################################################################

declare -A GO_PACKAGES
declare -A SYS_PACKAGES

ERROR_LOG=.dependencies.log

# Go gettable packages
GO_PACKAGES=(
    [golf]="github.com/karrick/golf"
    [git]="gopkg.in/src-d/go-git.v4"
    [gitlog]="github.com/tsuyoshiwada/go-gitlog"
    [tests]="github.com/cweill/gotests"
    [mergo]="github.com/imdario/mergo"
    [input]="github.com/tcnksm/go-input"
    [billy]="-u gopkg.in/src-d/go-billy.v4/..."
    ["packr-bin"]="-u github.com/gobuffalo/packr/v2/packr2"
    [packr]="-u github.com/gobuffalo/packr/v2"
)

# Install a go package
# Args:
#  - 1: The human package's name to be installed
#  - 2: The package's url
function install_go_package {
    NAME=$1
    URL=$2
    SANE_URL=$(echo $URL | sed 's/\-u//')
    SANE_URL=$(echo $SANE_URL | sed 's/\/\.\.\.//')
    if [ -z "$(go list $SANE_URL)" ]; then
        INSTALLED_PACKAGES="${INSTALLED_PACKAGES} ${NAME}"
        printf "\r[+] Installing ${NAME} (@$URL)...\033[K"
        go get $URL > "$ERROR_LOG"
    else
        printf "\r[+] Package ${NAME} already installed (@$URL)\033[K"
    fi
}

function install_go_packages {
    # Install go packages
    for PKG in ${!GO_PACKAGES[@]}; do
        install_go_package "$PKG" "${GO_PACKAGES[$PKG]}"
    done
    printf "\rInstalled $(echo $INSTALLED_PACKAGES | wc -w)/${#GO_PACKAGES[@]} packages\033[K\n"
}

# Triggers system software sources to be fetched
function update_system_sources {
    echo "[+] Updating software sources..."
    sudo apt-get update > "$ERROR_LOG"
}

# Install system packages
# Args:
#  - 1: Human name of the package to install
#  - 2: Names of system packages
function install_system_package {
    NAME=$1
    shift
    PACKAGES=$*
    echo "[+] Installing ${NAME}..."
    for PKG in ${PACKAGES[@]}; do
        echo -e "\t- ${PKG}"
    done
    sudo apt-get install $PACKAGES > "$ERROR_LOG"
}

# Update software sources if any system package to be installed
if [[ ${#SYS_PACKAGES[@]} -gt 0 ]]; then
    update_system_sources
    for PKG in ${!SYS_PACKAGES[@]}; do
        install_system_package $PKG ${SYS_PACKAGES[$PKG]}
    done
fi

install_go_packages

#install_system_package libwebkit2gtk-4.0-dev libgtk3.0-cil-dev
#install_go_package github.com/zserge/webview