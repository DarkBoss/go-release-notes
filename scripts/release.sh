#!/usr/bin/env bash

printf "Release mode ?:\n"
printf "%s\n" " 1. (M)ajor"
printf "%s\n" " 2. (m)inor"
printf "%s\n" " 3. (r)evision"
printf "%s\n" " 4. (b)uild"
printf "%s\n" "------------"
printf " 0. Abort\n"

EXEC=""
CHANGELOG_CMD="changelog.sh"
BUMP_VERSION_CMD="bump_version.sh"
GIT_FLOW_CMD="git flow release"

mode=""
done=0

declare -A OPTIONS=(
    ["startOnly"]=0
    ["finishOnly"]=0
    ["dryRun"]=0
)
function abort {
    echo $*
    echo Aborted.
    exit 1
}

function parseMode {
    while [ -z "$mode" ]; do
        read -i "0" -p "-> Choice: " ANS
        case $ANS in
        1|M)
            mode="major"
            ;;
        2|m)
            mode="minor"
            ;;
        3|r)
            mode="rev"
            ;;
        4|b)
            mode="build"
            ;;
        0)
            abort
           ;;
        esac
    done

    echo "Starting $mode release..."
}

function parseArgs {
    while [ $# -gt 0 ]; do
        case $1 in
        -S|--start-only)
            OPTIONS[startOnly]=1
           ;;
        -F|--finish-only)
            OPTIONS[finishOnly]=1
           ;;
        -N|--dry-run)
            OPTIONS[dryRun]=1
           ;;
        *)
            abort "unknown options: $1"
            ;;
        esac
        shift
    done
}

parseArgs $*
parseMode

if [[ ${OPTIONS[dryRun]} == 1 ]]; then
    EXEC="echo "
fi

SCRIPT_DIR=$(dirname $0)

CHANGELOG_CMD="${EXEC} ${SCRIPT_DIR}/${CHANGELOG_CMD}"
GIT_FLOW_CMD="${EXEC} ${GIT_FLOW_CMD}"
BUMP_VERSION_CMD="${EXEC} ${SCRIPT_DIR}/${BUMP_VERSION_CMD}"

echo "* Used commands:"
echo "  - GIT_FLOW_COMMAND: ${GIT_FLOW_CMD}"
echo "  - BUMP_VERSION_CMD: ${BUMP_VERSION_CMD}"
echo "  - CHANGELOG_CMD: ${CHANGELOG_CMD}"

read -e -i "n" -p "Bump version ? [yN] " ANS
case $ANS in
y)
    ${BUMP_VERSION_CMD} "$mode"
    VERSION=$(cat .version)
    git checkout .version
   ;;
*)
esac

read -e -i "n" -p "Start gitflow release ? [yN] " ANS
case $ANS in
y)
    ${GIT_FLOW_CMD} start "$VERSION"
   ;;
*)
esac

${BUMP_VERSION_CMD} "$mode"

read -e -i "n" -p "Write changelog ? [yN] " ANS
case $ANS in
y)
    ${CHANGELOG_CMD} "$VERSION"
   ;;
*)
esac

read -e -i "" -p "When done preparing the release, push a key... " ANS


read -e -i "n" -p "Finish gitflow release ? [yN] " ANS
case $ANS in
y)
    ${GIT_FLOW_CMD} finish
   ;;
*)
esac