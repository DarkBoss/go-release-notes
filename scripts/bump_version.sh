#!/usr/bin/env bash

VERSION=${VERSION:-0.0.1.0}
VERSION_FILE=${VERSION_FILE:-.version}
BUMP_MODE=$1

ALLOWED_BUMP_MODES="major minor rev build"
function usage {
    echo "$0 <part>"
    echo -e "parts: ${ALLOWED_BUMP_MODES}"
    exit 1
}

if [ -z "${BUMP_MODE}" ]; then usage $0; fi

if [ -z "$(cat ${VERSION_FILE})" ]; then
    echo ${VERSION} | tee ${VERSION_FILE}
    exit 0
fi

VERSION_MAJOR=$(cat $VERSION_FILE | cut -d. -f 1)
VERSION_MINOR=$(cat $VERSION_FILE | cut -d. -f 2)
VERSION_REV=$(cat $VERSION_FILE | cut -d. -f 3)
VERSION_BUILD=$(cat $VERSION_FILE | cut -d. -f 4)

case $BUMP_MODE in
major)
    VERSION_MAJOR=$(( ${VERSION_MAJOR} + 1 ))
   ;;
minor)
    VERSION_MINOR=$(( ${VERSION_MINOR} + 1 ))
   ;;
rev)
    VERSION_REV=$(( ${VERSION_REV} + 1 ))
   ;;
build)
    VERSION_BUILD=$(( ${VERSION_BUILD} + 1 ))
   ;;
*)
    echo "allowed parts: ${ALLOWED_BUMP_MODES}"
    exit 1
esac

mv ${VERSION_FILE} ${VERSION_FILE}.old
echo "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_REV}.${VERSION_BUILD}" | tee $VERSION_FILE