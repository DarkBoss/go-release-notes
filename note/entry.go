package note

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/DarkBoss/go-release-notes/gitflow"
)

// EntryFlag represents a single entry flag
type EntryFlag uint8

// EntryFlags represents passed-in flags
type EntryFlags uint32

// ReduceEntryFlags reduce a list of single flags into a single multi-flag variable
func ReduceEntryFlags(flags ...EntryFlag) EntryFlags {
	ret := uint32(DefaultEntryFlags)
	for _, f := range flags {
		ret = ret | uint32(f)
	}
	return EntryFlags(ret)
}

// EntryFlags
const (
	NoEntryFlags      EntryFlag  = iota
	EntryVisible                 = 1 << iota
	DefaultEntryFlags EntryFlags = EntryVisible
)

// Entry represents a single entry in the release note
type Entry struct {
	Flow           *gitflow.Flow `json:"flow"`
	Type           gitflow.Type  `json:"type"`
	Flags          EntryFlags    `json:"flags"`
	Hash           string        `json:"hash"`
	LinkedEntity   string        `json:"linkedEntity"`
	LinkedEntityId string        `json:"linkedEntityId"`
	Category       string        `json:"category"`
	Author         string        `json:"author"`
	Date           time.Time     `json:"date"`
	FormattedDate  string        `json:"formattedDate"`
	Message        string        `json:"message"`
}

// NewEntry constructs a new empty entry
func NewEntry() *Entry {
	return NewEntryWithValues(nil, gitflow.NoType, "", DefaultEntryFlags, "", "", "", "", time.Now(), "")
}

// NewEntryWithValues constructs a new entry with supplied values.
// No assertions is made, so the input must be correct.
func NewEntryWithValues(Flow *gitflow.Flow, Type gitflow.Type, Hash string, Flags EntryFlags, LinkedEntity, linkedEntityId, Category, Author string, Date time.Time, Message string) *Entry {
	return &Entry{
		Flow,
		Type,
		Flags,
		fmt.Sprintf("%8.8s", Hash),
		LinkedEntity,
		linkedEntityId,
		Category,
		Author,
		Date,
		Date.Format("2006-01-02 15:04:05"),
		strings.TrimSpace(Message),
	}
}
