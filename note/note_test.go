package note

import (
	"strings"
	"testing"

	"bitbucket.org/DarkBoss/go-release-notes/git"
	"bitbucket.org/DarkBoss/go-release-notes/gitflow"
)

// TestCategorize checks the categorization mecanism,
// by checking every gitflow type categorization.
func TestCategorize(t *testing.T) {
	type args struct {
		b *gitflow.Branch
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"default category", args{gitflow.NewBranch(gitflow.NoType, "")}, "technical task"},
		{"bugfix category", args{gitflow.NewBranch(gitflow.TypeBugfix, "")}, "defect fixing"},
		{"hotfix category", args{gitflow.NewBranch(gitflow.TypeHotfix, "")}, "production defect fixing"},
		{"feature category", args{gitflow.NewBranch(gitflow.TypeFeature, "")}, "feature implementation"},
		{"support category", args{gitflow.NewBranch(gitflow.TypeSupport, "")}, "feature support"},
		{"release category", args{gitflow.NewBranch(gitflow.TypeRelease, "")}, "release"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Categorize(tt.args.b); got != tt.want {
				t.Errorf("Categorize() = %v, want %v", got, tt.want)
			} else {
				t.Logf("Categorize('%s') = %v", tt.args.b.String(), got)
			}
		})
	}
}

func TestNote_FindNeededFilesLocation(t *testing.T) {
	repo, err := git.InitEmptyRepository("test")
	if err != nil {
		t.Fatalf("Note.FindNeededFilesLocation() error %s", err.Error())
	}
	if wt, err := repo.Worktree(); err != nil {
		t.Fatalf("Note.FindNeededFilesLocation() error %s", err.Error())
	} else {
		if projFile, err := wt.Filesystem.Create(defaultProjectInfos.GetDefaultNoteProjectFile()); err != nil {
			t.Errorf("Note.FindNeededFilesLocation() error %s", err.Error())
		} else {
			t.Logf("Note.FindNeededFilesLocation() = %v", projFile)
		}
		if tplFile, err := wt.Filesystem.Create(defaultProjectInfos.GetDefaultNoteTemplateFile()); err != nil {
			t.Errorf("Note.FindNeededFilesLocation() error %s", err.Error())
		} else {
			t.Logf("Note.FindNeededFilesLocation() = %v", tplFile)
		}
	}
	note := NewNote(GetDefaultProjectInfos(), gitflow.NewFlowWithDefaults(), []*Entry{})
	note.repository = repo
	tests := []struct {
		name    string
		this    *Note
		want    map[string]string
		wantErr bool
	}{
		{"should find needed files", note, map[string]string{"project": "", "template": ""}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.this.FindNeededFilesLocation()
			if (err != nil) != tt.wantErr {
				t.Errorf("Note.FindNeededFilesLocation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(strings.TrimSpace(got["project"])) == 0 {
				t.Errorf("Note.FindNeededFilesLocation() empty project location = %v", got)
			} else {
				t.Logf("Found project file: %s", got["project"])
			}
			if len(strings.TrimSpace(got["template"])) == 0 {
				t.Errorf("Note.FindNeededFilesLocation() empty project template = %v", got)
			} else {
				t.Logf("Found template file: %s", got["template"])
			}
		})
	}
}
