package note

import "bitbucket.org/DarkBoss/go-release-notes/gitflow"

// GetCategory returns the associated category of a specific git flow
func GetCategory(t gitflow.Type) string {
	c, ok := GetCategories()[uint8(t)]
	if !ok {
		return GetDefaultCategory()
	}
	return c
}

// GetDefaultCategory returns the default category associated to the
// "NoType" flow, uncategorized commits will have this category
func GetDefaultCategory() string {
	return GetCategories()[uint8(gitflow.NoType)]
}

// GetCategories returns all known gitflow categories (kanban)
func GetCategories() map[uint8]string {
	return map[uint8]string{
		uint8(gitflow.NoType):      "technical task",
		uint8(gitflow.TypeBugfix):  "defect fixing",
		uint8(gitflow.TypeHotfix):  "production defect fixing",
		uint8(gitflow.TypeFeature): "feature implementation",
		uint8(gitflow.TypeSupport): "feature support",
		uint8(gitflow.TypeRelease): "release",
	}
}

// Categorize a specific gitflow branch.
func Categorize(b *gitflow.Branch) string {
	if b == nil {
		return GetCategory(gitflow.NoType)
	}
	ret := GetCategory(b.Type())
	return ret

}
