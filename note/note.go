package note

import (
	"bitbucket.org/DarkBoss/go-release-notes/resources"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/tcnksm/go-input"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	goTemplate "text/template"
	"time"

	"bitbucket.org/DarkBoss/go-release-notes/format/data"
	"bitbucket.org/DarkBoss/go-release-notes/format/template"
	"bitbucket.org/DarkBoss/go-release-notes/log"
	"bitbucket.org/DarkBoss/go-release-notes/options"

	"bitbucket.org/DarkBoss/go-release-notes/errors"
	"bitbucket.org/DarkBoss/go-release-notes/format"
	"bitbucket.org/DarkBoss/go-release-notes/git"
	"bitbucket.org/DarkBoss/go-release-notes/gitflow"
)

var ErrNoValidRepository = errors.New("no valid git repository")

// ProjectInfo represents the configuration of the current project.
type ProjectInfos struct {
	Name    string `json:"name"`
	Version string `json:"version"`

	ConfigPath   []string `json:"_"`
	TemplatePath []string `json:"_"`
	NoteAuthor   string   `json:"noteAuthor"`
	Team         string   `json:"team"`
}

func NewProjectInfos(name, version string, configPath, templatePath []string, noteAuthor, team string) *ProjectInfos {
	return &ProjectInfos{
		name, version, configPath, templatePath, noteAuthor, team,
	}
}

var defaultProjectInfos = NewProjectInfos(
	options.DefaultOptionProjectName,
	options.DefaultOptionProjectVersion,
	[]string{".release-note", options.DefaultOptionProjectFileName},
	[]string{".release-note", options.DefaultOptionProjectTemplateName},
	"release-guy 1442",
	"dream team",
)

func GetDefaultProjectInfos() *ProjectInfos {
	p := &ProjectInfos{}
	*p = *defaultProjectInfos
	return p
}

// GetDefaultNoteProjectFile returns the default project's configuration file location
func (this *ProjectInfos) GetDefaultNoteProjectFile() string {
	return filepath.Join(this.ConfigPath...)
}

// GetDefaultNoteTemplateFile returns the default project's template file location
func (this *ProjectInfos) GetDefaultNoteTemplateFile() string {
	return filepath.Join(this.TemplatePath...)
}

// Note represents the generated release note,
// all data contained within this struct is given to
// the template while expanding it.
type Note struct {
	Project     *ProjectInfos  `json:"project"`
	Flow        *gitflow.Flow  `json:"flow"`
	Flags       EntryFlags     `json:"flags"`
	Commits     []*Entry       `json:"commits"`
	Stats       *data.Stats    `json:"stats"`
	Format      *format.Format `json:"format"`
	AuthorWords string         `json:"authorWords"`

	Styles  map[string]string `json:"styles"`
	Scripts map[string]string `json:"scripts"`

	Template *template.Template `json:"-"`
	Log      *git.Log           `json:"-"`

	repository *git.Repository
	options    *options.AppOptions
}

// NewNote constructs a new Note object with basic values.
func NewNote(proj *ProjectInfos, flow *gitflow.Flow, entries []*Entry) *Note {
	n := &Note{
		proj,
		flow,
		DefaultEntryFlags,
		entries,
		data.NewStats(),
		nil,
		"",
		map[string]string{
			"dataTables":    "styles/jquery.dataTables.min.css",
			"semanticFonts": "styles/semantic-fonts.css",
			"semantic":      "styles/semantic.min.css",
			"core":          "styles/core.css",
		},
		map[string]string{
			"dataTables": "scripts/jquery.dataTables.min.js",
			"jquery":     "scripts/jquery.min.js",
			"semantic":   "scripts/semantic.min.js",
			"chart":      "scripts/Chart.bundle.min.js",
			"core":       "scripts/core.js",
		},
		format.GetDefaultTemplate(),
		nil,
		nil,
		options.GetApp(),
	}
	n.Format = format.GetDefaultFormat()
	if entries == nil {
		n.Commits = []*Entry{}
	}
	if n.Flow == nil {
		n.Flow = gitflow.NewFlowWithDefaults()
	}
	return n
}

// Add an entry to the Note
func (this *Note) Add(e *Entry) {
	this.Commits = append(this.Commits, e)
}

// Remove an entry from the Note
func (this *Note) Remove(predicate func(*Entry) bool) {
	i := 0
	toRemove := []*Entry{}
	for _, e := range this.Commits {
		if predicate(e) {
			toRemove = append(toRemove, e)
		}
	}
	for _, tre := range toRemove {
		i = this.IndexOf(tre)
		this.Commits = append(this.Commits[:i], this.Commits[i+1:]...)
	}
}

// Find the index of a specific Entry
func (this *Note) IndexOf(e *Entry) int {
	for i, elem := range this.Commits {
		if e == elem {
			return i
		}
	}
	return -1
}

// Get a specific Entry in the Note
func (this *Note) Get(i int64) *Entry {
	return this.Commits[i]
}

// Filter Commits by applying a predicate
// - If true returned, keep Entry
// - If false returned, skip Entry
func (this *Note) Filter(predicate func(*Entry) bool) []*Entry {
	ret := []*Entry{}
	for _, e := range this.Commits {
		if predicate(e) {
			ret = append(ret, e)
		}
	}
	return ret
}

// FindCommits with a predicate function:
// - If true returned, keep Entry
// - If false returned, skip Entry
func (this *Note) FindCommits(predicate func(*Entry) bool) []*Entry {
	ret := []*Entry{}
	for _, e := range this.Commits {
		if predicate(e) {
			ret = append(ret, e)
		}
	}
	return ret
}

// Fill the Note with git commits
func (this *Note) Fill(commits []*git.Commit) error {
	type CommitterData struct {
		id         uint16
		value      uint16
		complement string
		style      *data.StatStyle
	}
	committers := map[string]*CommitterData{}
	categories := map[string]*CommitterData{}
	var committerId, categoryId = uint16(0), uint16(0)
	this.Stats.NumDefectsFixed = 0
	for _, c := range commits {
		if committer, ok := committers[c.Author.Name]; !ok {
			committers[c.Author.Name] = &CommitterData{committerId, 1, c.Author.Email, nil}
			committerId++
		} else {
			committer.value += 1
		}
		_, flow, _, _, _, _ := this.CreateEntryFromCommit(c)
		flowName := Categorize(flow)
		if flow != nil && (flow.Type() == gitflow.TypeBugfix || flow.Type() == gitflow.TypeHotfix) {
			this.Stats.NumDefectsFixed++
		} else {
			this.Stats.NumFeatures++
		}
		if category, ok := categories[flowName]; !ok {
			categories[flowName] = &CommitterData{categoryId, 1, "", nil}
			categoryId++
		} else {
			category.value += 1
		}
	}
	for k, committer := range committers {
		compls := []string{}
		if len(committer.complement) > 0 {
			compls = append(compls, committer.complement)
		}
		this.Stats.CreateCommitter(committer.id, k, committer.value, this.GetCommitterColor(committer.id), committer.complement)
	}
	for k, category := range categories {
		compls := []string{}
		if len(category.complement) > 0 {
			compls = append(compls, category.complement)
		}
		this.Stats.CreateCategory(category.id, k, category.value, this.GetCategoryColor(category.id), compls...)
	}
	this.Stats.NumCommits = int64(len(commits))
	return nil
}

// Generate the Note, writing it to file if necessary
func (this *Note) Generate() (string, error) {
	outputBuf := bytes.NewBufferString("")
	if this.options.UseProjectTemplate {
		// generate using configured template
		this.Format.Writer = format.NewTemplateWriter()
		this.Format.Writer.UpdateTemplate(this.Template)
	}
	for k, n := range this.Styles {
		data, err := resources.Get(n)
		if err != nil {
			return "", err
		}
		this.Styles[k] = string(data)
	}
	for k, n := range this.Scripts {
		data, err := resources.Get(n)
		if err != nil {
			return "", err
		}
		this.Scripts[k] = string(data)
	}
	if err := this.AskAuthorWords(); err != nil {
		return "", err
	}
	wd := data.NewData()
	if err := wd.Capture(this); err != nil {
		return "", err
	}
	if this.options.DumpNote {
		if data, err := json.MarshalIndent(this, "", "\t"); err != nil {
			return "", err
		} else {
			fmt.Fprintf(os.Stderr, "%s\n", data)
			os.Exit(1)
		}
	}
	if outName, err := this.GetOutputFilename(); err != nil {
		errors.Print(err)
	} else if err = this.Format.Writer.Write(outputBuf, wd); err != nil {
		return "", err
	} else if err := ioutil.WriteFile(outName, outputBuf.Bytes(), 0777); err != nil {
		return outputBuf.String(), nil
	} else if this.options.VerboseLevel >= log.VerboseLow {
		fmt.Printf("Written %s\n", outName)
	}
	return outputBuf.String(), nil
}

// GetOutputFilename returns the expanded output name
func (this *Note) GetOutputFilename() (string, error) {
	outputFilename := this.options.OutputFilenameFormat
	outputBuf := bytes.NewBufferString("")
	t := goTemplate.Must(goTemplate.New("temp").Parse(outputFilename))
	e := t.Execute(outputBuf, *this) //this.builddata.Data())
	return outputBuf.String(), e
}

// FullAuthorName returns the full author name of a specific commit
// (name + email).
func (this *Note) FullAuthorName(commit *git.Commit) string {
	if options.GetApp().DisplayLongAuthors {
		return fmt.Sprintf("%s <%s>", commit.Author.Name, commit.Author.Email)
	}
	return commit.Author.Name
}

// GetFlowType returns the type of flow of a specific branch
func (this *Note) GetFlowType(b *gitflow.Branch) gitflow.Type {
	ret := gitflow.NoType
	if b != nil {
		ret = b.Type()
	}
	return ret
}

// GetFlowBranch returns the configured branch for a specific flow type
func (this *Note) GetFlowBranch(t gitflow.Type) *gitflow.Branch {
	return this.Flow.GetBranch(t)
}

// CreateEntryFromCommit creates a new entry in this note, with values
// taken from a git commit.
func (this *Note) CreateEntryFromCommit(commit *git.Commit, flags ...EntryFlag) (e *Entry, flow *gitflow.Branch, branch, linkedEntity, linkedEntityId, message string) {
	ok := false
	var err error
	ok, flow, branch, linkedEntity, linkedEntityId, message, err = this.Flow.Extract(commit.Message)
	if err != nil {
		errors.Print(err)
	}
	if !ok {
		message = commit.Message
	}
	e = this.CreateEntry(
		commit.Hash.String(),
		message,
		this.FullAuthorName(commit),
		Categorize(flow),
		this.GetFlowType(flow),
		linkedEntity,
		linkedEntityId,
		commit.Author.When)
	return e, flow, branch, linkedEntity, linkedEntityId, message
}

// CreateEntry creates a new entry in this note, with
// supplied values.
func (this *Note) CreateEntry(commitHash, commitMessage, fullAuthorName, category string, flowType gitflow.Type, linkedEntity, linkedEntityId string, t time.Time, flags ...EntryFlag) *Entry {
	e := NewEntryWithValues(this.Flow, flowType, commitHash, ReduceEntryFlags(flags...), linkedEntity, linkedEntityId, category, fullAuthorName, t, commitMessage)
	this.Commits = append(this.Commits, e)
	return e
}

// CaptureGitRepo captures the git repository informations and fills
// the ProjectInfo struct with found data, or default values
func (this *Note) CaptureGitRepo(repository *git.Repository) error {
	this.Project.Name = repository.Name()
	this.Project.Version = "1.0.0.0"
	this.repository = repository
	var err error
	if this.Log, err = git.NewLog(this.repository); err != nil {
		return err
	}
	if neededFiles, err := this.FindNeededFilesLocation(); err != nil {
		return err
	} else if err = this.CaptureProjectConfig(neededFiles); err != nil {
		return err
	} else if err = this.CaptureProjectTemplate(neededFiles); err != nil {
		return err
	}
	if len(this.Project.Name) == 0 {
		this.Project.Name = filepath.Base(repository.Path())
	}

	if this.repository.Name() == "." {
		if path, err := os.Getwd(); err != nil {
			return err
		} else {
			this.repository.SetPath(path)
			this.repository.SetName(filepath.Base(this.repository.Path()))
		}
	}

	if this.Project.Name == "." {
		this.Project.Name = this.repository.Name()
	}
	return nil
}

// FindNeededFilesLocation tries to filter the git repository and
// extract locations from it.
func (this *Note) FindNeededFilesLocation() (map[string]string, error) {
	type LocationList = []string
	type LocationMap = map[string]string
	type InfoMap = map[string]os.FileInfo
	neededFiles := map[string][]string{
		"project":  []string{this.repository.GetPathInsideRepo(this.Project.GetDefaultNoteProjectFile())},
		"template": []string{this.repository.GetPathInsideRepo(this.Project.GetDefaultNoteTemplateFile())},
	}
	foundPaths := LocationMap{}
	foundInfos := InfoMap{}
	// create default values
	for neededKey := range neededFiles {
		foundPaths[neededKey] = ""
		foundInfos[neededKey] = nil
	}
	if this.repository == nil {
		return nil, ErrNoValidRepository
	}
	// try to find needed files
	if _, err := this.repository.Filter(string(os.PathSeparator), func(path string, info os.FileInfo) bool {
		for neededKey, possiblePaths := range neededFiles {
			for _, possiblePath := range possiblePaths {
				if path == possiblePath {
					foundInfos[neededKey] = info
					foundPaths[neededKey] = path
					return true
				}
			}
		}
		return false
	}); err != nil {
		return nil, err
	}
	// print missing needed files
	for k, v := range foundPaths {
		if len(v) == 0 {
			errors.Print(fmt.Errorf("%s: failed to find needed repository file, possible paths: %v", k, neededFiles[k]))
		}
	}
	return foundPaths, nil
}

// CaptureProjectTemplate captures the project's configuration file
// and extracts it's structure to the ProjectInfo type.
func (this *Note) CaptureProjectConfig(foundPaths map[string]string) error {
	this.Project.Name = this.options.ProjectName
	this.Project.NoteAuthor = this.options.NoteAuthor
	this.Project.Team = this.options.TeamName
	this.Project.TemplatePath = filepath.SplitList(this.options.ProjectTemplateName)
	this.Project.Version = this.options.ProjectVersion
	if projectPath, _ := foundPaths["project"]; len(projectPath) > 0 {
		if data, err := this.repository.ReadFile(projectPath); err != nil {
			errors.Print(fmt.Errorf("%s: %s", projectPath, err))
		} else {
			if err = json.Unmarshal(data, &this.Project); err != nil {
				errors.Print(fmt.Errorf("%s: %s", projectPath, err))
			} else if this.options.VerboseLevel >= log.VerboseLow {
				fmt.Printf("[+] %s: found project configuration file @ %s\n\t-> %v\n", "project", projectPath, this.Project)
			}
		}
	}
	return nil
}

// CaptureProjectTemplate captures the project's template file content
// by scanning the supplied git repository.
func (this *Note) CaptureProjectTemplate(foundPaths map[string]string) error {
	if templatePath, _ := foundPaths["template"]; len(templatePath) > 0 {
		if data, err := this.repository.ReadFile(templatePath); err != nil {
			errors.Print(fmt.Errorf("%s: %s", templatePath, err))
		} else if len(strings.TrimSpace(string(data))) == 0 {
			errors.Print(fmt.Errorf("%s: template is empty", templatePath))
		} else if err = this.Template.Parse(string(data)); err != nil {
			return err
		} else if this.options.VerboseLevel >= log.VerboseLow {
			fmt.Printf("[+] %s: found project configuration file @ %s\n\t-> %s\n", "template", templatePath, data)
		}
	}
	return nil
}

func (this *Note) GetProjectFilename() string {
	return filepath.Join(this.options.ProjectFolderName, this.options.ProjectFileName)
}

const defaultTagNameFormat = `([0-9\.\-_]+)`

var defaultTagNameRule = regexp.MustCompile(defaultTagNameFormat)

func GetTagNameFormat() string {
	return defaultTagNameFormat
}

func GetTagNameRule() *regexp.Regexp {
	return defaultTagNameRule
}

func GetTagName(s string) string {
	matches := defaultTagNameRule.FindAllStringSubmatch(s, -1)
	ret := ""
	if len(matches) > 0 {
		ret = matches[0][0]
	}
	return ret
}

func (this *Note) GetCommitterColors() []*data.StatStyle {
	return []*data.StatStyle{
		data.NewStatStyle("rgba(255, 99, 132, 0.2)", "rgba(255,99,132,1)"),
		data.NewStatStyle("rgba(54, 162, 235, 0.2)", "rgba(54, 162, 235, 1)"),
		data.NewStatStyle("rgba(255, 206, 86, 0.2)", "rgba(255, 206, 86, 1)"),
		data.NewStatStyle("rgba(75, 192, 192, 0.2)", "rgba(75, 192, 192, 1)"),
		data.NewStatStyle("rgba(153, 102, 255, 0.2)", "rgba(153, 102, 255, 1)"),
		data.NewStatStyle("rgba(255, 159, 64, 0.2)", "rgba(255, 159, 64, 1)"),
	}
}
func (this *Note) GetCommitterColor(i uint16) *data.StatStyle {
	colorSets := this.GetCommitterColors()
	colorSet := colorSets[int(i)%len(colorSets)]
	return colorSet.Clone()
}

func (this *Note) GetCategoryColors() []*data.StatStyle {
	return []*data.StatStyle{
		data.NewStatStyle("rgba(255, 99, 132, 0.2)", "rgba(255,99,132,1)"),
		data.NewStatStyle("rgba(54, 162, 235, 0.2)", "rgba(54, 162, 235, 1)"),
		data.NewStatStyle("rgba(255, 206, 86, 0.2)", "rgba(255, 206, 86, 1)"),
		data.NewStatStyle("rgba(75, 192, 192, 0.2)", "rgba(75, 192, 192, 1)"),
		data.NewStatStyle("rgba(153, 102, 255, 0.2)", "rgba(153, 102, 255, 1)"),
		data.NewStatStyle("rgba(255, 159, 64, 0.2)", "rgba(255, 159, 64, 1)"),
	}
}

func (this *Note) GetCategoryColor(i uint16) *data.StatStyle {
	colorSets := this.GetCategoryColors()
	colorSet := colorSets[int(i)%len(colorSets)]
	return colorSet.Clone()
}

func (this *Note) ListRecentTags() (*git.TagRefList, error) {
	tags, err := this.repository.FilterTags(func(tag *git.TagRef) bool {
		return true
	})
	if err != nil {
		return nil, err
	}
	if err = tags.Sort(git.SortByName | git.SortReverse); err != nil {
		return nil, err
	}
	if tags.Len() > 0 {
		log.Printf(log.VerboseLow, "[+] found %d project tags, choosing: %s - %s\n", tags.Len(), tags.First().Name, tags.First().Hash.String())
	}
	return tags, nil
	//this.options.Find(&git.LogOptions{From: from})
}

func (this *Note) AskAuthorWords() error {
	if this.options.Interactive && len(strings.TrimSpace(this.AuthorWords)) == 0 {
		ui := input.UI{Writer: os.Stdout, Reader: os.Stdin}
		if data, err := ui.Ask("Author / team words ?\nIf the team has something special to say, will appear at the very top of\nthe generated release note", &input.Options{Default: this.AuthorWords}); err != nil {
			return err
		} else {
			this.AuthorWords = strings.TrimSpace(data)
		}
	}
	return nil
}
