package app

import (
	"bitbucket.org/DarkBoss/go-release-notes/log"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	"bitbucket.org/DarkBoss/go-release-notes/options"
	"github.com/karrick/golf"
)

func getApp(v interface{}) *App {
	a := v.(*App)
	if a == nil {
		panic("App instance should NOT be nil!")
	}
	return a
}

func init() {
	RegisterOptions()
}

func RegisterOptions() {
	registerAndCheckErr := func(name string, short rune, long string, defaultVal interface{}, usage string, handler options.OptionHandler) {
		if _, err := options.RegisterOption(name, short, long, defaultVal, usage, handler); err != nil {
			panic(err)
		}
	}

	// bool flags
	registerAndCheckErr("help", 'h', "help", false, "show this help screen", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.ShowHelp = o.GetBool()
		return nil
	}))

	registerAndCheckErr("version", 'V', "version", false, "show software version", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.ShowVersion = o.GetBool()
		return nil
	}))

	registerAndCheckErr("no-interactive", 'I', "no-interactive", false, "disable any terminal interaction", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.Interactive = !o.GetBool()
		return nil
	}))

	// verbose options
	registerAndCheckErr("quiet", 'q', "quiet", false, "show no log messages", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		if o.GetBool() {
			getApp(v).options.VerboseLevel = log.VerboseNone
		}
		return nil
	}))
	registerAndCheckErr("shy", 0, "shy", false, "show additional log messages", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		if o.GetBool() {
			getApp(v).options.VerboseLevel = log.VerboseLow
		}
		return nil
	}))
	registerAndCheckErr("verbose", 'v', "verbose", false, "show more additionnal log messages", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		if o.GetBool() {
			getApp(v).options.VerboseLevel = log.VerboseMedium
		}
		return nil
	}))
	registerAndCheckErr("talkative", 0, "talkative", false, "show all log messages", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		if o.GetBool() {
			getApp(v).options.VerboseLevel = log.VerboseHigh
		}
		return nil
	}))

	// output format options
	registerAndCheckErr("output-filename-format", 'o', "output", options.DefaultOptionOutputFilenameFormat, "change the output filename format", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.OutputFilenameFormat = o.GetString()
		return nil
	}))
	registerAndCheckErr("format", 'f', "format", options.DefaultOptionOutputFormat, "Change the output format. This option is overridden by project template", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.OutputFormat = o.GetString()
		return nil
	}))

	registerAndCheckErr("changeDir", 'C', "change-directory", ".", "change directory before scanning repository", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.RepoPath = o.GetString()
		return nil
	}))

	// generator options
	registerAndCheckErr("generate project", 'G', "generate-config", options.DefaultOptionGenerateConfig, "generate a basic project configuration in the current directory", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.GenerateProjectConfig = o.GetBool()
		return nil
	}))

	// project configuration options
	registerAndCheckErr("project name", 'N', "project-name", options.DefaultOptionProjectName, "override the inferred project name", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.ProjectName = o.GetString()
		return nil
	}))
	registerAndCheckErr("project version", 0, "project-version", options.DefaultOptionProjectVersion, "override the inferred project version", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.ProjectVersion = o.GetString()
		return nil
	}))
	registerAndCheckErr("project folder name", 0, "project-folder-name", options.DefaultOptionProjectFolderName, "override the project folder name", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.ProjectFolderName = o.GetString()
		return nil
	}))
	registerAndCheckErr("project file name", 0, "project-file-version-name", options.DefaultOptionProjectFileName, "override the project filename", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.ProjectFileName = o.GetString()
		return nil
	}))
	registerAndCheckErr("project file template name", 0, "project-file-template-name", options.DefaultOptionProjectTemplateName, "override the project template filename", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.ProjectTemplateName = o.GetString()
		return nil
	}))

	registerAndCheckErr("no project template", 0, "no-project-template", !options.DefaultOptionUseProjectTemplate, "disable usage of user configured project template (use format one)", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.UseProjectTemplate = !o.GetBool()
		return nil
	}))
	registerAndCheckErr("no project", 0, "no-project", !options.DefaultOptionUseProjectConfig, "disable project files config override", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.UseProjectConfig = !o.GetBool()
		return nil
	}))
	registerAndCheckErr("no long authors", 0, "no-long-authors", !options.DefaultOptionLongAuthor, "disable long author names in release note", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.DisplayLongAuthors = !o.GetBool()
		return nil
	}))

	registerAndCheckErr("author", 'a', "author", options.DefaultOptionNoteAuthor, "define release author name", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.NoteAuthor = o.GetString()
		return nil
	}))

	registerAndCheckErr("author-words", 0, "author-words", options.DefaultOptionProjectTeamWords, "defines author words block at the top of the generated note", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.TeamWords = o.GetString()
		return nil
	}))

	registerAndCheckErr("team", 't', "team", options.DefaultOptionProjectTeamName, "define project team name", options.NewStringOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.TeamName = o.GetString()
		return nil
	}))

	// dump & debug options
	registerAndCheckErr("dump-config", 'D', "dump-config", false, "dump the active configuration and exit", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.DumpConfig = o.GetBool()
		return nil
	}))

	registerAndCheckErr("dump-note", 0, "dump-note", false, "dump the note data before writing it to disk and exit", options.NewBoolOpt(func(o *options.Option, v interface{}) error {
		getApp(v).options.DumpNote = o.GetBool()
		return nil
	}))

}
type AppData struct {
	Name   string
	Era    string
	Desc   string
	Author string
}

var appData = AppData{
	"my_app",
	"2019-2021",
	"My awesome description...",
	"its-me-user",
}

func Configure(name, era, desc, author string) *AppData {
	appData.Name = name
	appData.Era = era
	appData.Desc = desc
	appData.Author = author
	return &appData
}

func GetData() *AppData {
	return &appData
}

func prepareUsage(out io.Writer, opts ...*options.Option) error {
	if out == nil {
		out = os.Stderr
	}
	if _, err := fmt.Fprintf(out, "Usage of %s:\n\n", appData.Name); err != nil {
		return err
	}
	if _, err := 	fmt.Fprintf(out, "\t%s\n\n", strings.Join(strings.Split(appData.Desc, "\n"), "\n\t")); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(out, "* Copyright\n\n"); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(out, "\t%s (@%s)\n\n", appData.Author, appData.Era); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(out, "* Synopsis\n\n"); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(out, "\t.%c%s [OPTIONS]\n\n", os.PathSeparator, appData.Name); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(out, "* Options\n\n"); err != nil {
		return err
	}
	for _, opt := range opts {
		if opt.Short != 0 {
			if _, err := fmt.Fprintf(out, "  -%c, --%s:\n\t\t%s\n\n", opt.Short, opt.Long, opt.Usage); err != nil {
				return err
			}
		} else {
			if _, err := fmt.Fprintf(out, "  --%s:\n\t\t%s\n\n", opt.Long, opt.Usage); err != nil {
				return err
			}
		}
	}
	return nil
}

func Usage(out io.Writer) {
	if err := prepareUsage(out, *options.GetAll()...); err != nil {
		panic(err)
	}
	os.Exit(1)
}

func parseCLI(activateData interface{}) (*options.AppOptions, error) {
	var declaredOptions = *options.GetAll()
	golf.Parse()
	var err error
	for _, o := range declaredOptions {
		if err = o.Handler.Activate(o, activateData); err != nil {
			return nil, err
		}
	}
	return options.GetApp(), nil
}

func parseUserConfig(cliConfig *options.AppOptions, filenames ...[]string) (*options.AppOptions, error) {
	// load json files
	var err error
	var ret = &options.AppOptions{}
	var found = false
	var path = ""
	if len(filenames) > 0 {
		firstFilename := options.ExpandPath(filenames[0]...)
		if path, found, err = options.FindExistingFile(filenames...); !found || err != nil {
			if err = cliConfig.Save(firstFilename); err != nil {
				return nil, fmt.Errorf("cannot create user default configuration, %s", err.Error())
			}
			if path, found, err = options.FindExistingFile(filenames...); !found || err != nil {
				return nil, err
			}
		}
		if err = ret.Load(path); err != nil {
			return nil, err
		}
	}
	return ret, err
}

// ParseOptions takes cli & user defined config option and agregates them
// into a single option struct.
// It's argument is an App instance, passed down to activated options.
func ParseOptions(activateData interface{}) (*options.AppOptions, error) {
	var err error
	var cliOptions, usrOptions *options.AppOptions

	if cliOptions, err = parseCLI(activateData); err != nil {
		return nil, err
	}
	locations := options.GetDefaultUserConfigLocations(cliOptions.ProjectFolderName)
	if usrOptions, err = parseUserConfig(cliOptions, locations ...); err != nil {
		return nil, err
	}
	if err := options.Update(*usrOptions, *cliOptions); err != nil {
		return nil, err
	}
	log.SetVerbose(options.GetApp().VerboseLevel)
	if options.GetApp().VerboseLevel >= log.VerboseLow {
		optStr, err := json.MarshalIndent(options.GetApp(), "", "  ")
		if err != nil {
			optStr = []byte(fmt.Sprintf("%+v", *options.GetApp()))
		}
		log.Printf(log.VerboseHigh, "[+] Parsed configuration: %s\n", optStr)
	}
	if options.GetApp().ShowHelp {
		Usage(nil)
	} else if options.GetApp().ShowVersion {
		fmt.Printf("%s\n", gVersion)
		os.Exit(1)
	}
	return options.GetApp(), nil
}
