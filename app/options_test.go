package app

import (
	"bitbucket.org/DarkBoss/go-release-notes/log"
	"bytes"
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/DarkBoss/go-release-notes/errors"
	"bitbucket.org/DarkBoss/go-release-notes/options"
)

func TestGetAppOptions(t *testing.T) {
	tests := []struct {
		name string
		want *options.AppOptions
	}{
		{"GetAppOptions should return valid global instance", options.GetApp()},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := options.GetApp(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAppOptions() = %v, want %v", got, tt.want)
			} else {
				t.Logf("GetAppOptions() = %v", got)
			}
		})
	}
}

func TestAppOptions_Merge(t *testing.T) {
	type fields struct {
		RepoPath              string
		OutputFilenameFormat  string
		OutputFormat          string
		VerboseLevel          log.VerboseLevel
		ProjectName           string
		ProjectVersion        string
		DisplayLongAuthors    bool
		UseProjectConfig      bool
		UseProjectTemplate    bool
		ProjectFolderName     string
		ProjectFileName       string
		ProjectTemplateName   string
		GenerateProjectConfig bool
		NoteAuthor            string
		TeamName              string
	}
	type args struct {
		with []*options.AppOptions
	}
	fieldsEmpty := fields{RepoPath: ""}
	fieldsFull := fields{RepoPath: "test"}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *options.AppOptions
		wantErr bool
	}{
		{"should overwrite target field with filled source value", fieldsEmpty, args{[]*options.AppOptions{&options.AppOptions{RepoPath: fieldsFull.RepoPath}}}, &options.AppOptions{RepoPath: fieldsFull.RepoPath}, false},
		{"should NOT overwrite target field with empty target value", fieldsFull, args{[]*options.AppOptions{&options.AppOptions{RepoPath: fieldsEmpty.RepoPath}}}, &options.AppOptions{RepoPath: fieldsFull.RepoPath}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &options.AppOptions{
				RepoPath:              tt.fields.RepoPath,
				OutputFilenameFormat:  tt.fields.OutputFilenameFormat,
				OutputFormat:          tt.fields.OutputFormat,
				VerboseLevel:          tt.fields.VerboseLevel,
				ProjectName:           tt.fields.ProjectName,
				ProjectVersion:        tt.fields.ProjectVersion,
				DisplayLongAuthors:    tt.fields.DisplayLongAuthors,
				UseProjectConfig:      tt.fields.UseProjectConfig,
				UseProjectTemplate:    tt.fields.UseProjectTemplate,
				ProjectFolderName:     tt.fields.ProjectFolderName,
				ProjectFileName:       tt.fields.ProjectFileName,
				ProjectTemplateName:   tt.fields.ProjectTemplateName,
				GenerateProjectConfig: tt.fields.GenerateProjectConfig,
				NoteAuthor:            tt.fields.NoteAuthor,
				TeamName:              tt.fields.TeamName,
			}
			if err := this.Merge(tt.args.with...); (err != nil) != tt.wantErr {
				t.Errorf("AppOptions.Merge() error = %v, wantErr %v", err, tt.wantErr)
			} else if !reflect.DeepEqual(this, tt.want) {
				t.Errorf("AppOptions.Merge() = %v, want %v", err, tt.want)
			} else {
				t.Logf("AppOptions.Merge() = %v", *this)
			}
		})
	}
}

func Test_prepareUsage(t *testing.T) {
	type args struct {
		opts []*options.Option
	}
	tests := []struct {
		name     string
		args     args
		checkOut func(out string) error
		wantErr  bool
	}{
		{
			"should generate non-empty string",
			args{[]*options.Option{}},
			func(out string) error {
				if len(out) > 0 {
					return nil
				}
				return errors.New("usage is empty")
			},
			false,
		}, {
			"should generate options",
			args{[]*options.Option{
				options.NewOption("help", 'h', "help", false, "", nil),
			}},
			func(out string) error {
				const needle = "-h, --help"
				if strings.Index(out, needle) > -1 {
					return nil
				}
				return errors.New("failed to find '" + needle + "'")
			},
			false,
		},
	}
	var err error
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			out := &bytes.Buffer{}
			if err := prepareUsage(out, tt.args.opts...); (err != nil) != tt.wantErr {
				t.Errorf("prepareUsage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			gotOut := out.String()
			if err = tt.checkOut(gotOut); (err != nil) != tt.wantErr {
				t.Errorf("prepareUsage() = %v, %s", gotOut, err.Error())
			}
		})
	}
}

func TestConfigure(t *testing.T) {
	type args struct {
		name   string
		era    string
		desc   string
		author string
	}
	tests := []struct {
		name string
		args args
		want *AppData
	}{
		{"data can be configured", args{"app", "era", "desc", "author"}, &AppData{"app", "era", "desc", "author"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Configure(tt.args.name, tt.args.era, tt.args.desc, tt.args.author); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Configure() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRegisterOptions(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{"help option should registered", "help"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if opt := options.GetAll().Find(func(o *options.Option) bool { return o.Name == tt.want}); opt == nil {
				t.Errorf("missing options '%s'", tt.want)
			}
		})
	}
}
