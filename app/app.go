package app

import (
	"bitbucket.org/DarkBoss/go-release-notes/log"
	"bitbucket.org/DarkBoss/go-release-notes/errors"
	"bitbucket.org/DarkBoss/go-release-notes/format"
	"bitbucket.org/DarkBoss/go-release-notes/git"
	"bitbucket.org/DarkBoss/go-release-notes/gitflow"
	"bitbucket.org/DarkBoss/go-release-notes/note"
	"bitbucket.org/DarkBoss/go-release-notes/options"
	"encoding/json"
	"fmt"
	"github.com/imdario/mergo"
	"github.com/tcnksm/go-input"
	git2 "gopkg.in/src-d/go-git.v4"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var gInstance *App

var gVersion string = "undefined"

func Version() string {
	return gVersion
}

type App struct {
	options *options.AppOptions
	repo    *git.Repository
	commits []*git.Commit
	note    *note.Note
	format  *format.Format
}

func NewApp(options *options.AppOptions) *App {
	return &App{
		options,
		nil,
		nil,
		note.NewNote(note.GetDefaultProjectInfos(), gitflow.NewFlowWithDefaults(), []*note.Entry{}),
		format.GetDefaultFormat(),
	}
}

func (this *App) SetOptions(o *options.AppOptions) error {
	this.options = o
	dfltProjectInfos := note.GetDefaultProjectInfos()
	usrProjectInfos := &note.ProjectInfos{
		o.ProjectName,
		o.ProjectVersion,
		dfltProjectInfos.ConfigPath,
		dfltProjectInfos.TemplatePath,
		o.NoteAuthor,
		o.TeamName,
	}
	projectInfos := &note.ProjectInfos{}
	if err := mergo.Merge(projectInfos, dfltProjectInfos, mergo.WithOverride); err != nil {
		return err
	}
	if err := mergo.Merge(projectInfos, usrProjectInfos, mergo.WithOverride); err != nil {
		return err
	}
	return nil
}

func (this *App) OpenRepository() error {
	var err error
	this.repo, err = git.OpenRepository(this.options.RepoPath)
	return err
}

func (this *App) SetRepository(repo *git.Repository) {
	this.repo = repo
}

func (this *App) Initialize() (err error) {
	if err = this.ChooseOutputFormat(); err != nil {
		return err
	}
	if this.repo == nil && len(this.options.RepoPath) > 0 {
		if err = this.OpenRepository(); err != nil {
			return err
		}
	}
	if this.repo != nil {
		if err = this.note.CaptureGitRepo(this.repo); err != nil {
			return err
		}
	}
	return err
}

func (this *App) ChooseOutputFormat() error {
	selectedOutputFormat := strings.ToLower(this.options.OutputFormat)
	availableFormats := format.GetSupportedFormats()
	availableFormatNames := format.GetSupportedFormatNames()
	selectedFormat, ok := availableFormats[selectedOutputFormat]
	if !ok {
		return fmt.Errorf("unsupported output format '%s', available: %v", selectedOutputFormat, strings.Join(availableFormatNames, ", "))
	}
	this.format = format.NewFormat(selectedFormat.Name, selectedFormat.Ext, selectedFormat.Writer)
	this.note.Format = this.format
	log.Printf(log.VerboseLow, "[+] Chosen output format: %v\n", this.format)
	return nil
}

func (this *App) Shutdown() error {
	return nil
}

func (this *App) Run() error {
	var err error
	if this.options.DumpConfig {
		js, _ := json.MarshalIndent(this.options, "", "\t")
		fmt.Fprintf(os.Stderr, "Active configuration: %s\n", string(js))
		os.Exit(1)
	}
	if this.options.VerboseLevel >= log.VerboseLow {
		log.Printf(log.VerboseMedium, "[+] Scanning repository '%s' (%s)\n", this.repo.Name(), this.repo.Path())
	}
	if this.options.GenerateProjectConfig {
		if err = this.GenerateProjectConfig(); err != nil {
			return err
		}
	} else {
		if err = this.FindCommits(); err != nil {
			return err
		}
		if err = this.note.Fill(this.commits); err != nil {
			errors.Print(err)
		}
		if _, err := this.note.Generate(); err != nil {
			return err
		}
	}
	return err
}

func (this *App) GenerateProjectConfig() error {
	var err error
	if this.options.Interactive == false {
		return errors.New("not an interactive tty, remove --no-interactive option!")
	}
	ui := input.UI{Writer: os.Stdout, Reader: os.Stdin}
	if this.note.Project.Name, err = ui.Ask("Project name ?", &input.Options{Default: this.note.Project.Name, Required: true, Loop: true}); err != nil {
		return err
	}
	if this.note.Project.Version, err = ui.Ask("Project version ?", &input.Options{Default: this.note.Project.Version, Required: true, Loop: true}); err != nil {
		return err
	}
	if this.note.Project.Team, err = ui.Ask("Project team ?", &input.Options{Default: this.note.Project.Team, Required: true, Loop: true}); err != nil {
		return err
	}
	path := ""
	if path, err = ui.Ask("Project template ?", &input.Options{Default: filepath.Join(this.note.Project.TemplatePath...), Required: true, Loop: true}); err != nil {
		return err
	}
	if this.note.Project.NoteAuthor, err = ui.Ask("Release note author ?", &input.Options{Default: this.note.Project.NoteAuthor, Required: true, Loop: true}); err != nil {
		return err
	}
	path = strings.TrimSpace(path)
	this.note.Project.TemplatePath = strings.Split(path, string(os.PathSeparator))

	this.note.Project.Name = strings.TrimSpace(this.note.Project.Name)
	this.note.Project.Version = strings.TrimSpace(this.note.Project.Version)
	this.note.Project.Team = strings.TrimSpace(this.note.Project.Team )
	this.note.Project.NoteAuthor = strings.TrimSpace(this.note.Project.NoteAuthor)

	if len(this.options.RepoPath) > 0 {
		path = filepath.Join(this.options.RepoPath, this.note.GetProjectFilename())
	} else {
		path = this.note.GetProjectFilename()
	}
	dir := filepath.Dir(path)
	if err = os.MkdirAll(dir, 0777); err != nil {
		return err
	}
	if data, err := json.MarshalIndent(this.note.Project, "", "\t"); err != nil {
		return err
	} else if err = ioutil.WriteFile(path, data, 0777); err != nil {
		return err
	}
	return nil
}

func (this *App) FindCommits() error {
	var err error
	var tags *git.TagRefList
	var fromHash git.Hash
	if tags, err = this.note.ListRecentTags(); err != nil {
		return err
	}
	if tags.Len() > 0 {
		fromHash = tags.First().TargetHash
	}
	var commits git.CommitIter
	if commits, err = this.note.Log.Find(&git2.LogOptions{Order: git2.LogOrderCommitterTime}); err != nil {
		return err
	}
	var keptCommits = []*git.Commit{}
	var errStop = errors.New("stop")
	f, err := os.Create("hashes.log")
	if err != nil {
		return err
	}
	fmt.Fprintf(f, "%40.40s\t%40.40s\t%40.40s\n", "Commit Hash", "Wanted Hash", "Match")
	if err = commits.ForEach(func(c *git.Commit) error {
		fmt.Fprintf(f, "%40.40s\t%40.40s\t%40t\n", c.Hash.String(), fromHash.String(), c.Hash.String() == fromHash.String())
		if !fromHash.IsZero() && c.Hash.String() == fromHash.String() {
			return errStop
		}
		keptCommits = append(keptCommits, c)
		return nil
	}); err != nil && err != errStop {
		return err
	}
	log.Printf(log.VerboseMedium, "-> %d commits kept", len(keptCommits))
	this.commits = keptCommits
	return nil
}

func Create(meta *AppData, initNow bool) (app *App, err error) {
	var opt *options.AppOptions
	if gInstance != nil {
		return nil, fmt.Errorf("App already instanciated!")
	}
	if meta != nil {
		appData = *meta
	}
	gInstance = NewApp(options.GetApp())
	app = gInstance
	if opt, err = ParseOptions(gInstance); err != nil {
		return nil, err
	}
	app.SetOptions(opt)
	if initNow {
		err = app.Initialize()
	}
	return app, err
}

func Shutdown() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Fprintf(os.Stderr, "[panic] %v\n", r)
		}
	}()
	if gInstance != nil {
		if err := gInstance.Shutdown(); err != nil {
			fmt.Fprintf(os.Stderr, "[error] %s\n", err.Error())
		}
	}
}
