### Release 0.1.2.0

#### Changeset since 0.1.1.0

  * acd4a72 Add repository guard in Note
  * 7ba9c40 Merge branch feature/improve-readme-add-cli-examples into develop
  * 4cbe3f4 Merge branch bugfix/various-cli-option-issues into develop
  * 56d64e1 Improve message cleanup process & add tests
  * 7569d23 Merge branch bugfix/cannot-create-default-config-on-windows into develop
  * 29ab69a Implement release script
  * 2b95b2c Improve changelog script
  * a7981fc Merge branch feature/improve-command-usage-screen into develop
  * 02a2821 Merge branch feature/add-graphs-to-show-statistics-of-release into develop
  * 5dae6a4 Merge branch feature/grab-only-changes-sinces-last-tag into develop
  * 833e3ff Fix formattedDate being empty
  * c325f9f Fix wrong project name defined when current dir is .
  * 2c8f582 Merge branch feature/add-basic-stylesheet-for-html-note into develop

### Release 0.1.1.0

#### Changeset since 0.1.0.0

  * 352977a Merge branch bugfix/linked-entity-not-displayed-in-release-note into develop
  * eafedd1 Improve changelog, add view mode and help
  * 0e50f85 Rename duplicate option tag
  * 4a95039 Merge tag 0.1.0.0 into develop

### Release 0.1.0.0

#### Changeset 

  * 3091962 Improve changelog
  * 3623c85 Fix bump script to Protect against wrong part name
  * d85dec4 Add changelog script
  * 17e8841 Create version file and update script
  * 7e43727 Generate project configuration interactively
  * 5193a80 Support CSV format
  * 247f4f2 Fix format choice not working
  * a6512cc Disable project template with cli config
  * eb9edac Generate pretty printed user configuration
  * 48c1eac Fix project names for example repos
  * 8ff388c Merge branch feature/change-release-note-config-files-names into develop
  * 6746ec5 Merge branch feature/load-global-config-from-user-home-directory into develop
  * 7722447 Document scripts
  * f80721f Document note package
  * a6ebec3 Document errors package
  * fbb6cf5 Document git package
  * b44f809 Allow filtering of git repository for windows-like paths
  * b2dc388 Add CSV example repository
  * 02cdbe7 Add option to disable project loading
  * bf1a0fa Use text/template instead of html/template
  * fec3556 Refactor ill-named function
  * 3752a47 Use project template while generating note
  * 03aff3a Improve readme & update usage
  * 045b65a Merge branch feature/load-project-from-repository into develop
  * 89419ce Implement some format/html and config tests
  * ea293de Refactor & clean packages
  * 5dfb26b Fix linting errors: rename release_note package -> note
  * bf6f7f9 Add locale attributes to DefaultTemplate
  * 940507e Add vscode project to gitignore
  * 20382f6 Refactor NoteFormat & Category
  * 5beebfa Fix build of TemplateWriter tests & remove BSON format support
  * 79dedf3 Merge branch bugfix/display-category-column into develop
  * 3c5d65b Improve error detection for HTMLNoteWriter test
  * 7a29abd Merge branch feature/generate-basic-release-note into develop
  * 8bba821 Merge branch feature/improve-git-handling into develop
  * 7cbf33a Read git log and add options
  * 2a37038 Improve dependency installation script
  * 3cca999 Improve readme
  * b07d2e4 Improve makefile
  * 8a26a8b Move example repository to sub-folder
  * 3fb68f7 Create build script
  * a2b244c Create dependency installation script
  * 697c173 Add AppOptions & App classes
  * c12a35e Provide first example repository
  * 6ed41dd Initial commit
