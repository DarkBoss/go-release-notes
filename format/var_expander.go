package format

import (
	"fmt"
	"regexp"
)

type VarExpander struct {
	format *regexp.Regexp
}

type VarMap = map[string]interface{}

func NewVarExpander() *VarExpander {
	return &VarExpander{regexp.MustCompile(`\$\{([\w\-_0-9\.:]+)\}`)}
}

func (this *VarExpander) Expand(content []byte, vars VarMap) []byte {
	return this.format.ReplaceAllFunc(content, func(original []byte) []byte {
		matches := this.format.FindSubmatch(original)
		if matches != nil && len(matches) > 1 {
			return []byte(fmt.Sprintf("%v", vars[string(matches[1])]))
		}
		return original
	})
}

func (this *VarExpander) ExpandString(content string, vars VarMap) string {
	return string(this.Expand([]byte(content), vars))
}
