package data

import (
	"bitbucket.org/DarkBoss/go-release-notes/gitflow"
	"encoding/json"
)

type Data struct {
	Stats       *Stats                 `json:"stats"`
	Commits     []*DataEntry           `json:"commits"`
	Project     map[string]interface{} `json:"project"`
	Flow        *gitflow.Flow          `json:"flow"`
	Format      map[string]interface{} `json:"format"`
	Options     map[string]interface{} `json:"options"`
	Styles      map[string]string      `json:"styles"`
	Scripts     map[string]string      `json:"scripts"`
	AuthorWords string                 `json:"authorWords"`
}

func NewData() *Data {
	return &Data{}
}

func CaptureData(note interface{}) (*Data, error) {
	wd := NewData()
	return wd, wd.Capture(note)
}

func (this *Data) Capture(note interface{}) error {
	e := &Data{}
	if is, err := json.Marshal(note); err != nil {
		return err
	} else {
		if err = json.Unmarshal(is, &e); err != nil {
			return err
		}
		*this = *e
	}
	return nil
}
