package data

import "reflect"

type StatStyle struct {
	Background string	`json:"background"`
	Border     string	`json:"border"`
}

func NewStatStyle(bg string, border string) *StatStyle {
	return &StatStyle{bg, border}
}

func (this *StatStyle) Clone() *StatStyle {
	ret := &StatStyle{}
	*ret = *this
	return ret
}

func (this *StatStyle) FromMap(style map[string]string) {
	rv := reflect.ValueOf(this)
	for k, v := range style {
		if f := rv.FieldByName(k); !f.IsNil() {
			f.SetString(v)
		}
	}
}

func (this *StatStyle) ToMap() map[string]string {
	rt := reflect.TypeOf(this)
	rv := reflect.Indirect(reflect.ValueOf(this))
	ret := map[string]string{}
	for i := 0; i < rv.NumField(); i++ {
		if f := rv.Field(i); !f.IsNil() {
			ret[rt.Field(i).Name] = f.String()
		}
	}
	return ret
}

func GetStyle(id uint16) {

}
