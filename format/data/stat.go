package data

type Stats struct {
	Committers StatEntryList `json:"committers"`
	Categories StatEntryList `json:"categories"`
	NumCommits int64 `json:"numCommits"`
	NumFeatures int64 `json:"numFeatures"`
	NumDefectsFixed int64 `json:"numDefectsFixed"`
}

func NewStats() *Stats {
	return &Stats{
		StatEntryList{},
		StatEntryList{},
		0,
		0,
		0,
	}
}

func (this *Stats) CreateCommitter(id uint16, name string, value uint16, style *StatStyle, complement ...string) *StatEntry {
	e := NewStatEntry(id, name, value, style, complement...)
	this.Committers = append(this.Committers, e)
	return this.Committers[len(this.Committers)-1]
}

func (this *Stats) CreateCategory(id uint16, name string, value uint16, style *StatStyle, complement ...string) *StatEntry {
	e := NewStatEntry(id, name, value, style, complement...)
	this.Categories = append(this.Categories, e)
	return this.Categories[len(this.Categories)-1]
}