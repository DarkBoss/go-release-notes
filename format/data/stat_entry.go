package data

import "strings"

type StatEntry struct {
	Id uint16
	Name  string
	Value interface{}
	Complement string
	Style *StatStyle
}

type StatEntryList = []*StatEntry

func NewStatEntry(id uint16, name string, value interface{}, style *StatStyle, complement ...string) *StatEntry {
	return &StatEntry{id, name, value, strings.Join(complement, " "), style}
}
