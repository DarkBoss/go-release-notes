package data

import "bitbucket.org/DarkBoss/go-release-notes/gitflow"

type DataEntry struct {
	Type           gitflow.Type `json:"type"`
	Hash           string       `json:"hash"`
	Date           string       `json:"date"`
	FormattedDate  string       `json:"formattedDate"`
	Author         string       `json:"author"`
	Category       string       `json:"category"`
	LinkedEntity   string       `json:"linkedEntity"`
	LinkedEntityId string       `json:"linkedEntityId"`
	Message        string       `json:"message"`
}

