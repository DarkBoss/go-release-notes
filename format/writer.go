package format

import (
	"bitbucket.org/DarkBoss/go-release-notes/format/data"
	"bitbucket.org/DarkBoss/go-release-notes/format/template"
	"io"
)

type Writer interface {
	UpdateTemplate(t *template.Template)
	Write(w io.Writer, v *data.Data) error

}
