package format

var formats = FormatMap{
	"html": NewFormat("HTML", "html", NewTemplateWriter()),
	"json": NewFormat("JSON", "json", NewJSONWriter()),
	"csv": NewFormat("CSV", "csv", NewCSVWriter()),
}

var defaultFormat = GetFormat("html")

func GetFormat(n string) *Format {
	return formats[n]
}

func GetFormats() FormatMap {
	return formats
}

func GetDefaultFormat() *Format {
	return defaultFormat
}

type Format struct {
	Name   string
	Ext    string
	Writer Writer
}

func NewFormat(name, ext string, w Writer) *Format {
	return &Format{name, ext, w}
}

// A map of note writers
type FormatMap map[string]*Format

// Get known note writers
func GetSupportedFormats() FormatMap {
	return formats
}

func GetSupportedFormatNames() []string {
	names := []string{}
	for _, format := range GetSupportedFormats() {
		names = append(names, format.Name)
	}
	return names
}

func GetSupportedFormatExtensions() []string {
	names := []string{}
	for _, format := range GetSupportedFormats() {
		names = append(names, format.Ext)
	}
	return names
}
