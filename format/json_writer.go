package format

import (
	"bitbucket.org/DarkBoss/go-release-notes/format/data"
	"encoding/json"
	"io"
)

func NewJSONWriter() *JSONWriter {
	return &JSONWriter{}
}

// This note writer allows note export to JSON format
type JSONWriter struct {
	Writer
}

func (this *JSONWriter) Write(w io.Writer, note *data.Data) error {
	if data, err := json.MarshalIndent(note, "", "\t"); err != nil {
		return err
	} else {
		w.Write(data)
		return nil
	}
}
