package template

import (
	"bytes"
	"strings"
	"text/template"
)

type Template struct {
	template *template.Template
	errors   []error
	funcs    template.FuncMap
}

func NewTemplate(name string) *Template {
	t := &Template{template.New(name), []error{}, template.FuncMap{
		"replace": func(what, by, in string) string {
			return strings.Replace(in, what, by, -1)
		},
	}}
	t.template.Parse(`{{template "document" .}}`)
	t.template.Funcs(t.funcs)
	return t
}

func (this *Template) Parse(content string) error {
	_, e := this.template.Parse(content)
	return e
}

func (this *Template) ParseFiles(names ...string) error {
	_, e := this.template.ParseFiles(names...)
	return e
}

func (this *Template) pushError(e error) error {
	this.errors = append(this.errors, e)
	return e
}

func (this *Template) Create(name, content string) error {
	return this.Add(NewPartial(name, content))
}

func (this *Template) Add(partials ...*Partial) error {
	var e error
	for _, p := range partials {
		p.Funcs(this.funcs)
		if _, e = p.Parse(); e != nil {
			this.pushError(e)
		} else {
			p.GetHandle().Funcs(this.funcs)
			_, e = this.template.AddParseTree(p.GetHandle().Name(), p.GetTree())
		}
	}
	return e
}

func (this *Template) GetTemplate() *template.Template {
	return this.template
}

func (this *Template) ExpandMap(data map[string]interface{}) (string, *template.Template, error) {
	buf := bytes.NewBufferString("")
	if e := this.template.Execute(buf, data); e != nil {
		return "", nil, this.pushError(e)
	}
	return buf.String(), this.template, nil
}

func (this *Template) ExpandStruct(data interface{}) (string, *template.Template, error) {
	buf := bytes.NewBufferString("")
	if e := this.template.Execute(buf, data); e != nil {
		return "", nil, this.pushError(e)
	}
	return buf.String(), this.template, nil
}

func (this *Template) JoinErrors(sep string) string {
	buf := bytes.NewBufferString("")
	for _, e := range this.errors {
		if buf.Len() == 0 {
			buf.WriteString(sep)
		}
		buf.WriteString(e.Error())
	}
	return buf.String()
}

func (this *Template) GetErrors() []error {
	return this.errors
}

func (this *Template) HasErrors() bool {
	return len(this.errors) > 0
}

func (this *Template) SetFunc(name string, fn func(t *template.Template) Template) {
	this.funcs[name] = fn
	this.template.Funcs(this.funcs)
}

func (this *Template) GetFuncs() template.FuncMap {
	return this.funcs
}

func (this *Template) Must() *template.Template {
	if this.HasErrors() {
		panic(this.JoinErrors("\n"))
	}
	return this.template
}

func (this *Template) Clone() (*template.Template, error) {
	if this.template == nil {
		panic(nil)
	}
	return this.template.Clone()
}
