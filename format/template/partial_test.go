package template

import (
	"bytes"
	"reflect"
	"testing"
	"text/template"
	"text/template/parse"
)

func TestNewPartial(t *testing.T) {
	type args struct {
		name    string
		content string
	}
	tests := []struct {
		name string
		args args
		want *Partial
	}{
		{
			"constructor works",
			args{"partial1", "im a partial!"},
			&Partial{"partial1", "im a partial!", false, nil, nil, nil}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewPartial(tt.args.name, tt.args.content); got.Equals(tt.want) {
				t.Errorf("NewPartial() = %v, want %v", got, tt.want)
			} else {
				t.Logf("NewPartial() = %v", got)
			}
		})
	}
}

func TestPartial_IsParsed(t *testing.T) {
	tests := []struct {
		name string
		this *Partial
		want bool
	}{
		{"isParsed should return false when not yet parsed", &Partial{"", "", false, nil, nil, nil}, false},
		{"isParsed should return true when parsed", &Partial{"", "", true, nil, nil, nil}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.IsParsed(); got != tt.want {
				t.Errorf("Partial.IsParsed() = %v, want %v", got, tt.want)
			} else {
				t.Logf("Partial.IsParsed() = %v", got)
			}
		})
	}
}

func TestPartial_GetHandle(t *testing.T) {
	tpl := template.New("test")
	tests := []struct {
		name string
		this *Partial
		want *template.Template
	}{
		{"GetHandle should return invalid instance when not yet parsed", &Partial{"", "", true, nil, nil, nil}, nil},
		{"GetHandle should return valid instance when parsed", &Partial{"", "", true, tpl, nil, nil}, tpl},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.GetHandle(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Partial.GetHandle() = %v, want %v", got, tt.want)
			} else {
				t.Logf("Partial.GetHandle() = %v", got)
			}
		})
	}
}

func TestPartial_GetContent(t *testing.T) {
	tests := []struct {
		name string
		this *Partial
		want string
	}{
		{"GetContent should return original content when not yet parsed", &Partial{"", "test", false, nil, nil, nil}, "test"},
		{"GetContent should return original content even when parsed", &Partial{"", "test", true, nil, nil, nil}, "test"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.GetContent(); got != tt.want {
				t.Errorf("Partial.GetContent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPartial_GetName(t *testing.T) {
	tests := []struct {
		name string
		this *Partial
		want string
	}{
		{"GetName should return name", &Partial{"test", "", false, nil, nil, nil}, "test"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.GetName(); got != tt.want {
				t.Errorf("Partial.GetName() = %v, want %v", got, tt.want)
			} else {
				t.Logf("Partial.GetName() = %v", got)
			}
		})
	}
}

func TestPartial_GetTree(t *testing.T) {
	tests := []struct {
		name string
		this *Partial
		want *parse.Tree
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.GetTree(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Partial.GetTree() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPartial_Reset(t *testing.T) {
	type args struct {
		content string
	}
	tests := []struct {
		name string
		this *Partial
		args args
		want *Partial
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.this.Reset(tt.args.content)
			if !tt.this.Equals(tt.want) {
				t.Errorf("Partial.Reset() = %v, want %v", tt.this, tt.want)
			} else {
				t.Logf("Partial.Reset() = %v", tt.this)
			}
		})
	}
}

func TestPartial_Parse(t *testing.T) {
	tpl := template.Must(template.New("test").Parse("hello!"))
	tests := []struct {
		name    string
		this    *Partial
		want    *template.Template
		wantErr bool
	}{
		{"Parse should behave just like golang's templates", NewPartial("test", "hello!"), tpl, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.this.Parse()
			if (err != nil) != tt.wantErr {
				t.Errorf("Partial.Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			bufGot := bytes.NewBufferString("")
			bufWant := bytes.NewBufferString("")
			data := map[string]interface{}{}
			if got == nil && tt.want != nil {
				t.Errorf("Partial.Parse() = %v, want %v", got, tt.want)
			} else if err = got.Execute(bufGot, data); err != nil {
				t.Errorf("Partial.Parse() error: %s", err.Error())
			} else if err = tt.want.Execute(bufWant, data); err != nil {
				t.Errorf("Partial.Parse() error: %s", err.Error())
			} else if bufGot.String() != bufWant.String() {
				t.Errorf("Partial.Parse() = %v, want %v", bufGot.String(), bufWant.String())
			} else {
				t.Logf("Partial.Parse() = %v", bufGot.String())
			}
		})
	}
}
