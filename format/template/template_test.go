package template

import (
	"text/template"
	"reflect"
	"testing"
)

func TestNewTemplate(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want *Template
	}{
		{"constructor works", args{"test"}, &Template{template.New("test"), []error{}, template.FuncMap{}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewTemplate(tt.args.name); got == nil || got.template == nil || got.errors == nil || got.funcs == nil {
				t.Errorf("NewTemplate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplate_Parse(t *testing.T) {
	type args struct {
		content string
	}
	tests := []struct {
		name    string
		this    *Template
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.this.Parse(tt.args.content); (err != nil) != tt.wantErr {
				t.Errorf("Template.Parse() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTemplate_ParseFiles(t *testing.T) {
	type args struct {
		names []string
	}
	tests := []struct {
		name    string
		this    *Template
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.this.ParseFiles(tt.args.names...); (err != nil) != tt.wantErr {
				t.Errorf("Template.ParseFiles() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTemplate_pushError(t *testing.T) {
	type args struct {
		e error
	}
	tests := []struct {
		name    string
		this    *Template
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.this.pushError(tt.args.e); (err != nil) != tt.wantErr {
				t.Errorf("Template.pushError() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTemplate_Create(t *testing.T) {
	type args struct {
		name    string
		content string
	}
	tests := []struct {
		name    string
		this    *Template
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.this.Create(tt.args.name, tt.args.content); (err != nil) != tt.wantErr {
				t.Errorf("Template.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTemplate_Add(t *testing.T) {
	type args struct {
		partials []*Partial
	}
	tests := []struct {
		name    string
		this    *Template
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.this.Add(tt.args.partials...); (err != nil) != tt.wantErr {
				t.Errorf("Template.Add() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTemplate_GetTemplate(t *testing.T) {
	tests := []struct {
		name string
		this *Template
		want *template.Template
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.GetTemplate(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Template.GetTemplate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplate_ExpandMap(t *testing.T) {
	type args struct {
		data map[string]interface{}
	}
	tests := []struct {
		name    string
		this    *Template
		args    args
		want    string
		want1   *template.Template
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.this.ExpandMap(tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("Template.ExpandMap() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Template.ExpandMap() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Template.ExpandMap() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestTemplate_ExpandStruct(t *testing.T) {
	type args struct {
		data interface{}
	}
	tests := []struct {
		name    string
		this    *Template
		args    args
		want    string
		want1   *template.Template
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.this.ExpandStruct(tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("Template.ExpandStruct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Template.ExpandStruct() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Template.ExpandStruct() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestTemplate_JoinErrors(t *testing.T) {
	type args struct {
		sep string
	}
	tests := []struct {
		name string
		this *Template
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.JoinErrors(tt.args.sep); got != tt.want {
				t.Errorf("Template.JoinErrors() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplate_GetErrors(t *testing.T) {
	tests := []struct {
		name string
		this *Template
		want []error
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.GetErrors(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Template.GetErrors() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplate_HasErrors(t *testing.T) {
	tests := []struct {
		name string
		this *Template
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.HasErrors(); got != tt.want {
				t.Errorf("Template.HasErrors() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplate_SetFunc(t *testing.T) {
	type args struct {
		name string
		fn   func(t *template.Template) Template
	}
	tests := []struct {
		name string
		this *Template
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.this.SetFunc(tt.args.name, tt.args.fn)
		})
	}
}

func TestTemplate_GetFuncs(t *testing.T) {
	tests := []struct {
		name string
		this *Template
		want template.FuncMap
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.GetFuncs(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Template.GetFuncs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplate_Must(t *testing.T) {
	tests := []struct {
		name string
		this *Template
		want *template.Template
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.Must(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Template.Must() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplate_Clone(t *testing.T) {
	tests := []struct {
		name    string
		this    *Template
		want    *template.Template
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.this.Clone()
			if (err != nil) != tt.wantErr {
				t.Errorf("Template.Clone() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Template.Clone() = %v, want %v", got, tt.want)
			}
		})
	}
}
