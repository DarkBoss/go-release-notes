package template

import (
	"fmt"
	"text/template"
	"text/template/parse"
)

type Partial struct {
	Name string
	Content string
	parsed bool
	handle *template.Template
	funcs template.FuncMap
	error error
}

func NewPartial(name string, content string) *Partial {
	return &Partial{Name: name, Content: content}
}


func (this *Partial) Equals(r *Partial) bool {
	sameName := this.Name != r.Name
	sameError := fmt.Sprintf("%v", this.error) != fmt.Sprintf("%v", r.error)
	sameContent := this.Content != r.Content
	sameParsed := this.parsed != r.parsed
	sameHandle := fmt.Sprintf("%v", this.handle) != fmt.Sprintf("%v", r.handle)
	return sameName && sameError && sameContent && sameParsed && sameHandle
}

func (this *Partial) IsParsed() bool {
	return this.parsed
}

func (this *Partial) GetHandle() *template.Template {
	return this.handle
}

func (this *Partial) GetContent() string {
	return this.Content
}

func (this *Partial) GetName() string {
	return this.Name
}

func (this *Partial) GetFuncs() template.FuncMap {
	return this.funcs
}

func (this *Partial) GetTree() *parse.Tree {
	if this.handle == nil {
		return nil
	}
	return this.handle.Tree
}

func (this *Partial) Reset(content string) {
	this.Content = content
	this.error = nil
	this.funcs = template.FuncMap{}
	this.handle = nil
	this.parsed = false
}

func (this *Partial) Parse() (*template.Template, error) {
	if !this.parsed {
		content := fmt.Sprintf(`{{define "%s"}}%s{{end}}`, this.Name, this.Content)
		this.handle, this.error = template.New(this.Name).Funcs(this.funcs).Parse(content)
		if this.error != nil {
			return nil, fmt.Errorf("%s: failed to parse partial: %s:\n%s", this.Name, this.error.Error(), this.Content)
		}
	}
	return this.handle, this.error
}

func (this *Partial) Funcs(funcs template.FuncMap) {
	this.funcs = funcs
	if this.handle != nil {
		this.handle.Funcs(this.funcs)
	}
}

