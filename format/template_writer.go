package format

import (
	"bitbucket.org/DarkBoss/go-release-notes/format/data"
	"bitbucket.org/DarkBoss/go-release-notes/format/template"
	"io"
)

// This note writer allows note export to HTML format
type TemplateWriter struct {
	Writer
	Template *template.Template
}

func NewTemplateWriter() *TemplateWriter {
	return &TemplateWriter{
		nil, GetDefaultTemplate(),
	}
}

func (this *TemplateWriter) UpdateTemplate(t *template.Template) {
	this.Template = t
}

func (this *TemplateWriter) Write(w io.Writer, note *data.Data) error {
	if s, _, e := this.Template.ExpandStruct(note); e != nil {
		return e
	} else if _, e = w.Write([]byte(s)); e != nil {
		return e
	}
	return nil
}
