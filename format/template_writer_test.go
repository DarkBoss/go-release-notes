package format

import (
	"testing"
)

func TestNewTemplateWriter(t *testing.T) {
	tests := []struct {
		name string
		want *TemplateWriter
	}{
		{"default constructor uses default template", &TemplateWriter{nil, GetDefaultTemplate()}},
	}
	if GetDefaultTemplate() == nil {
		t.Fatal("Default template is nil!")
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewTemplateWriter(); got == nil {
				t.Errorf("NewTemplateWriter() = %v, want %v: instanciation failed!", got, tt.want)
			} else if got.Template == nil {
				t.Errorf("NewTemplateWriter() = %v, want %v: template is nil!", got, tt.want)
			} else {
				t.Logf("NewTemplateWriter() = %v", *got)
			}
		})
	}
}
