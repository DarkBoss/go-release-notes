package format

import (
	"regexp"
	"testing"
)

var format = regexp.MustCompile(`\$\{([\w\-_0-9\.:]+)\}`)

func TestVarExpander_ExpandString(t *testing.T) {
	type fields struct {
		format *regexp.Regexp
	}
	type args struct {
		content string
		vars    map[string]interface{}
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		what string
	}{
		{"expand single variable", fields{format}, args{string(`${var_1}`), map[string]interface{}{"var_1": 42}}, string(`42`)},
		{"expand multiple variables", fields{format}, args{string(`${var_1} ${var_2}`), map[string]interface{}{"var_1": "hello", "var_2": "42"}}, string(`hello 42`)},
	}
	var content string
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &VarExpander{
				format: tt.fields.format,
			}
			content = this.ExpandString(tt.args.content, tt.args.vars)
			if content != tt.what {
				t.Errorf("Content failed to be expanded:\n\twanted: %s\n\tgot: %s\n", tt.what, content)
			} else {
				t.Logf("Expand '%s' with %v -> %s\n", tt.args.content, tt.args.vars, content)
			}
		})
	}
}
