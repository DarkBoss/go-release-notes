package format

import (
	"bitbucket.org/DarkBoss/go-release-notes/format/data"
	"encoding/csv"
	"io"
)

// This note writer allows note export to CSV format
type CSVWriter struct {
	Writer
}

func NewCSVWriter() *CSVWriter {
	return &CSVWriter{}
}

func (this *CSVWriter) CreateEntry(e *data.DataEntry) []string {
	return []string{
		e.Hash, e.Date, e.Author, e.Category, e.LinkedEntityId, e.Message,
	}
}

func (this *CSVWriter) Write(w io.Writer, note *data.Data) error {
	cw := csv.NewWriter(w)
	cw.Comma = ';'
	cw.UseCRLF = true
	headers := []string{
		"hash", "date", "author", "category", "linkedEntity", "message",
	}
	body := [][]string{}
	for _, e := range note.Commits {
		body = append(body, this.CreateEntry(e))
	}
	body = append([][]string{headers}, body...)
	if err := cw.WriteAll(body); err != nil {
		return err
	}
	return nil
}
