package format

import (
	"bitbucket.org/DarkBoss/go-release-notes/format/template"
	"bitbucket.org/DarkBoss/go-release-notes/resources"
)

const defaultTemplateWriterTemplateName = "main"

var defaultTemplateWriterTemplate *template.Template

func GetDefaultTemplateName() string {
	return defaultTemplateWriterTemplateName
}

func GetDefaultTemplate() *template.Template {
	if defaultTemplateWriterTemplate == nil {
		if content, err := resources.GetString("./templates/template.html"); err != nil {
			panic(err)
		} else {
			defaultTemplateWriterTemplate = template.NewTemplate(GetDefaultTemplateName())
			defaultTemplateWriterTemplate.Add(template.NewPartial("main", content))
			defaultTemplateWriterTemplate.Must()
		}
	}
	dup := &template.Template{}
	*dup = *defaultTemplateWriterTemplate
	return dup
}

func init() {
	GetDefaultTemplate()
}
