NAME=go-release-notes

BUILD_DIR=./build
TARGET=${BUILD_DIR}/${NAME}

SOURCES=main.go

BUILD_TOOL=packr2
VERSION=$(shell cat .version)

all:  ${TARGET}

build: ${TARGET}

${TARGET}: install_deps
	./scripts/bump_version.sh build
	${BUILD_TOOL} build -i -ldflags="-X bitbucket.org/DarkBoss/go-release-notes/app.gVersion=${VERSION}" -o $@ ${SOURCES}

package:
	go run scripts/package/main.go

prereqs: install_deps
	@test -d ${BUILD_DIR} || (mkdir -p ${BUILD_DIR}; echo "[+] Created build directory: ${BUILD_DIR}")

install_deps:
	@./scripts/install_deps.sh

run: ${TARGET}
	chmod +x ${TARGET}
	${TARGET} ${RUN_ARGS}

clean:
	${RM} -f ${TARGET}
	${BUILD_TOOL} clean .

re: clean build

.PHONY: clean run prereqs re install_deps build all